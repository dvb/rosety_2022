Contact info: jens.schwamborn@uni.lu or isabel.rosety@uni.lu

Please cite: https://www.nature.com/articles/s41531-023-00616-8 

Complete dataset can be found here: 

Data processing and plotting 

- Figure 1B - GCase activity
- Figure 4A - DA ELISA
- Figure 5A - DA genes
- Figure 5G - Propidium iodide staining_Cell cycle_Plotting
- Figure S3D - Seahorse
- Figure S4D - PI signaling genes
- Figure S7C - OrganoidSize
- Figure 5I - LaminB1 staining

