function  [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, ch4, PreviewPath, MetaTable)
    %vol(ch1, 0, 10000) Hoechst
    %vol(ch2, 0, 8000) 647 TH
    %vol(ch3, 0, 8000) 568 TUJ1
    %vol(ch4, 0, 5000) 488 FOXA2
    ObjectsThisOrganoid = table();
    %% Max projections
    %NucMax = max(ch1, [],3); imtool(NucMax,[])
    
    %% Segment Nuclei
%     NucDoG = imfilter(ch1, fspecial('gaussian', 21, 3) - fspecial('gaussian', 21, 5), 'symmetric'); % vol(NucDoG, 0, 100, 'hot')
%     NucMask = NucDoG > 100; %vol(NucMask, 0,1)
    ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 21, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 21, 3), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch3DoG, 0, 2000, 'hot')
    NucleiMask = ch1DoG > 150; %vol(NucleiMask)
    %NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    NucleiMask = bwareaopen(NucleiMask, 50);%vol(NucleiMask)
    ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch3LP, 0, 3000, 'hot')
    NucMaskHigh =  (ch1LP > 5000) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucMaskAlive)
    
    %NucMask = ch1 > 3000; vol(NucMask, 0,1)

   
    
    %% FOXA2
    ch4BlurSmall = imfilter(double(ch4), fspecial('gaussian', 21, 1), 'symmetric');%vol(ch3BlurSmall)
    ch4BlurBig = imfilter(double(ch4), fspecial('gaussian', 21, 3), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    ch4DoG = ch4BlurSmall - ch4BlurBig; %vol(ch3DoG, 0, 2000, 'hot')
    FOXA2Mask = ch4DoG > 30; %vol(NucleiMask)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    FOXA2Mask= FOXA2Mask & NucDil;
    FOXA2Mask = bwareaopen(FOXA2Mask, 200);%vol(FOXA2Mask)
    
    clear('ch4DoG') 
    %% TUJ1

    for p=1:size(ch3, 3)
        ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [15,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch3_FT * 1000, 0, 100, hot)

    Tuj1Mask = ch3_FT > 0.2;
    
 clear('ch3_FT') 
    %% TH
    
    for p=1:size(ch2, 3)
        ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [15,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch2_FT * 1000, 0, 100, hot)

    THMask = ch2_FT > 0.04;
    
    THMask = bwareaopen(THMask, 50);%vol(THMask)
    THandTuj1 =  THMask & Tuj1Mask; %vol(THandTuj1)
    
 clear('ch2_FT') 

 %% TH skeleton3D EPFL
    disp('Start skel')
    tic
    skelTH = Skeleton3D(THMask);
    toc
    disp('Skel done')
    %vol(skelTH, 0, 1)
    [AdjacencyMatrixTH, nodeTH, linkTH] = Skel2Graph3D(skelTH,0);                       
    %imtool(AdjacencyMatrixTH, [])
    NodeTH = zeros(size(THMask), 'uint8');
    NodeIdxs = vertcat(nodeTH(:).idx);
    NodeTH(NodeIdxs) = 1;
    %vol(NodeTH)    
    if size(NodeIdxs, 1) == 0
        return
    end
    NodeTHPreview = uint8(skelTH) + NodeTH + uint8(THMask); 
    NodeTHPreview2D = max(NodeTHPreview, [], 3);
    %it(NodeTHPreview2D)
    %vol(NodeTHPreview, 0, 3, 'jet')    
    NodeDegreeVectorTH = sum(AdjacencyMatrixTH, 1);

    ZeroNodeExplanationNeeded = 0;
    if ZeroNodeExplanationNeeded
        ZeroNodes = find(NodeDegreeVectorTH == 0);
        ZeroNodesLinIdx = vertcat(nodeTH(ZeroNodes).idx);
        ZeroNodeMask = zeros(size(THMaskClipped), 'uint8');
        ZeroNodeMask(ZeroNodesLinIdx) = 1; %vol(ZeroNodeMask)
        NodePreviewZeroCase = uint8(skelTH) + NodeMaskTH + 10*uint8(ZeroNodeMask) + uint8(THMask);
    end  

    %% TH Fragmentation

    % Define structuring element for surface detection
    Conn6 = strel('sphere', 1); % 6 connectivity
    % Detect surface
    SurfaceTH = THMask & ~(imerode(THMask, Conn6));
    %vol(SurfaceTH)
    
    
    %% Feture extraction
    Objects = table();
    Objects.OR_Idx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
    Objects.Barcode = {MetaTable.Barcode};
	Objects.Day = {MetaTable.Day};
    Objects.FOXA2Mask = sum(FOXA2Mask(:));
    Objects.Tuj1Mask = sum(Tuj1Mask(:));
    Objects.THMask = sum(THMask(:));
    Objects.THMFI = mean(ch2(THMask));
    Objects.TotalNucMask = sum(NucleiMask(:));
	Objects.NucDeadMask = sum(NucMaskHigh(:));
	Objects.Tuj1ByNucAlive = sum(Tuj1Mask(:)) / sum(NucMaskAlive(:));
	Objects.THByNucAlive = sum(THMask(:)) / sum(NucMaskAlive(:));
	Objects.THandTuj1ByTuj1 = sum(THandTuj1(:)) / sum(Tuj1Mask(:));
	Objects.THandTuj1ByNucAlive = sum(THandTuj1(:)) / sum(NucMaskAlive(:));
    Objects.FOXA2ByNucAlive = sum(FOXA2Mask(:)) / sum(NucMaskAlive(:));
	Objects.SkelTH = sum(skelTH(:));
    Objects.Nodes = size(nodeTH, 2);
	Objects.Links = size(linkTH, 2);
	Objects.NodesBySkel = size(nodeTH, 2)/sum(skelTH(:));
	Objects.LinksBySkel = size(linkTH, 2)/sum(skelTH(:));
    Objects.THFragmentation = sum(SurfaceTH(:)) / sum(THMask(:));
    
    
    
    %% Previews 
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');

    %RGB = cat(3, imadjust(max(chVimentin, [], 3), [0 0.3], [0 1]), imadjust(max(ch3, [], 3), [0 0.99], [0 1]), imadjust(max(ch1, [], 3), [0 0.3], [0 1]));
    %imtool(RGB)
    PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0.005 0.2]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
    PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]); %imtool(PreviewHoechst)
    PreviewHoechstMask = imoverlay2(imadjust(max(ch1,[],3),[0 0.2]), max(NucleiMask,[],3), [0 0 1]); %imtool(PreviewHoechstMask)
    PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0.005 ; 0.2]));
    PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)
    PreviewNucDead = imoverlay2(imadjust(max(ch1,[],3),[0.005 ; 0.2]), bwperim(max(NucMaskHigh,[],3)), [0 0 1]);
    PreviewNucDead = imoverlay2(PreviewNucDead, BarMask, [1 1 1]); %imtool(PreviewNucDead)
    PreviewNucAlive = imoverlay2(imadjust(max(ch1,[],3),[0.005 ; 0.2]), bwperim(max(NucMaskAlive,[],3)), [0 0 1]);
    PreviewNucAlive = imoverlay2(PreviewNucAlive, BarMask, [1 1 1]); %imtool(PreviewNucAlive)
    
    PreviewTuj1 = imoverlay2(imadjust(max(ch3,[],3),[0.005 ; 0.6]), bwperim(max(Tuj1Mask,[],3)), [1 0 0]);
    PreviewTuj1 = imoverlay2(PreviewTuj1, BarMask, [1 1 1]); %imtool(PreviewTuj1)
    PreviewTuj1Mask = imoverlay2(imadjust(max(ch3,[],3),[0.005 ; 0.6]), max(Tuj1Mask,[],3), [0 1 0]); %imtool(PreviewTuj1Mask)
    PreviewTuj1Raw = cat(3,imadjust(max(ch3, [], 3),[0.005 ; 0.6]),max(chEmpty,[],3), max(chEmpty,[],3)); 
    PreviewTuj1Raw = imoverlay2(PreviewTuj1Raw, BarMask, [1 1 1]); %imtool(PreviewTuj1Raw)
    
    PreviewFOXA2 = imoverlay2(imadjust(max(ch4,[],3),[0.01 ; 0.08]), bwperim(max(FOXA2Mask,[],3)), [0 1 0]);
    PreviewFOXA2 = imoverlay2(PreviewFOXA2, BarMask, [1 1 1]); %imtool(PreviewFOXA2)
    PreviewFOXA2Mask = imoverlay2(imadjust(max(ch4,[],3),[0.01 ; 0.08]), max(FOXA2Mask,[],3), [0 1 0]); %imtool(PreviewFOXA2Mask)
    PreviewFOXA2Raw = cat(3, max(chEmpty,[],3),imadjust(max(ch4, [], 3),[0.01 ; 0.08]),max(chEmpty,[],3)); 
    PreviewFOXA2Raw = imoverlay2(PreviewFOXA2Raw, BarMask, [1 1 1]); %imtool(PreviewFOXA2Raw)
    
    
    PreviewTH = imoverlay2(imadjust(max(ch2,[],3),[0.0001 ; 0.2]), bwperim(max(THMask,[],3)), [1 1 0]);
    PreviewTH = imoverlay2(PreviewTH, BarMask, [1 1 1]); %imtool(PreviewTH)
    PreviewTHMask = imoverlay2(imadjust(max(ch2,[],3),[0.0001 ; 0.2]), max(THMask,[],3), [1 0 0]); %imtool(PreviewTHMask)
    PreviewTHRaw = cat(3, imadjust(max(ch2, [], 3),[0.0001 ; 0.2]),imadjust(max(ch2,[],3),[0.0001 ; 0.2]),max(chEmpty,[],3));
    PreviewTHRaw  = imoverlay2(PreviewTHRaw , BarMask, [1 1 1]); %imtool(PreviewTHRaw)
    

    RGB_TH_TUJ1 = cat (3, imadjust(max(ch3,[],3),[0.05 ; 0.6]), imadjust(max(ch2, [], 3),[0.0001 ; 0.5]), max(chEmpty,[],3));
    RGB_GreenTH_RedTUJ1 = imoverlay2(RGB_TH_TUJ1, BarMask, [1 1 1]); %imtool(RGB_TH_TUJ1)
    
    RGB_TH_FOXA2 = cat (3, imadjust(max(ch2, [], 3),[0.0001 ; 0.5]), imadjust(max(ch4, [], 3),[0.01 ; 0.08]), max(chEmpty,[],3));
    RGB_RedTH_GreenFOXA2 = imoverlay2(RGB_TH_FOXA2, BarMask, [1 1 1]); %imtool(RGB_TH_FOXA2)
    
    RGB_TH_Hoechst = cat (3, imadjust(max(ch2, [], 3),[0.0001 ; 0.5]), max(chEmpty,[],3), imadjust(max(ch1,[],3),[0.005 0.2]));
    RGB_RedTH_Hoechst = imoverlay2(RGB_TH_Hoechst, BarMask, [1 1 1]); %imtool(RGB_RedTH_Hoechst)
    
    %% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '__', Objects.AreaName{:}{:}, '_']
    imwrite(PreviewHoechst, [PreviewPath, filesep, IdentityString, 'PreviewHoechst.png'])
    imwrite(PreviewHoechstMask, [PreviewPath, filesep, IdentityString, 'PreviewHoechstMask.png'])
    imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
    imwrite(PreviewNucDead, [PreviewPath, filesep, IdentityString, 'PreviewNucDead.png'])
    
    imwrite(PreviewTuj1, [PreviewPath, filesep, IdentityString, 'PreviewTuj1.png'])
    imwrite(PreviewTuj1Mask, [PreviewPath, filesep, IdentityString, 'PreviewTuj1Mask.png'])
    imwrite(PreviewTuj1Raw, [PreviewPath, filesep, IdentityString, 'PreviewTuj1Raw.png'])
    
    imwrite(PreviewTH, [PreviewPath, filesep, IdentityString, 'PreviewTH.png'])
    imwrite(PreviewTHMask, [PreviewPath, filesep, IdentityString, 'PreviewTHMask.png'])
    imwrite(PreviewTHRaw, [PreviewPath, filesep, IdentityString, 'PreviewTHRaw.png'])
    
    imwrite(PreviewFOXA2, [PreviewPath, filesep, IdentityString, 'PreviewFOXA2.png'])
    imwrite(PreviewFOXA2Mask, [PreviewPath, filesep, IdentityString, 'PreviewFOXA2Mask.png'])
    imwrite(PreviewFOXA2Raw, [PreviewPath, filesep, IdentityString, 'PreviewFOXA2Raw.png'])
    
    
    imwrite(RGB_GreenTH_RedTUJ1, [PreviewPath, filesep, IdentityString, 'RGB_GreenTH_RedTUJ1.png'])
    imwrite(RGB_RedTH_GreenFOXA2, [PreviewPath, filesep, IdentityString, 'RGB_RedTH_GreenFOXA2.png'])
    imwrite(RGB_RedTH_Hoechst, [PreviewPath, filesep, IdentityString, 'RGB_RedTH_Hoechst.png'])
end

    
