function  [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, ch4, PreviewPath, MetaTable)
    %vol(ch1, 0, 5000) Hoechst
    %vol(ch2, 0, 4000) 647 Sox2
    %vol(ch3, 0, 5000) 568 Edu 
    %vol(ch4, 0, 5000) 488 Ki67
    ObjectsThisOrganoid = table();
    %% Max projections
    %NucMax = max(ch1, [],3); imtool(NucMax,[])
    
    %% Segment Nuclei
%     NucDoG = imfilter(ch1, fspecial('gaussian', 21, 1) - fspecial('gaussian', 21, 3), 'symmetric'); % vol(NucDoG, 0, 100, 'hot')
%     NucMask = NucDoG > 100; %vol(NucMask, 0,1)

    ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 25, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 25, 5), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %figure; surf(fspecial('gaussian', 10, 5))
    ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch1DoG, 0, 2000, 'hot')
    NucleiMask = ch1DoG > 50; %vol(NucleiMask)
    %NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    NucleiMask = bwareaopen(NucleiMask, 50);%vol(NucleiMask)
    ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch3LP, 0, 3000, 'hot')
    NucMaskHigh =  (ch1LP > 2000) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucMaskAlive)
    %NucMask = ch1 > 3000; vol(NucMask, 0,1)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));

 
    %% Sox2
    Sox2DoG = imfilter(double(ch2), fspecial('gaussian', 101, 1) - fspecial('gaussian', 101, 21), 'symmetric');%vol(chEduDoG, 0, 500, 'hot')
    Sox2Mask = Sox2DoG > 250; %vol(NucleiMask)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));
    Sox2Mask= Sox2Mask & NucDil;
    Sox2Mask = imclose(squeeze(Sox2Mask), strel('disk', 5));
    Sox2Mask = bwareaopen(Sox2Mask, 1200);
    
    %% Edu
    EduDoG = imfilter(double(ch3), fspecial('gaussian', 101, 1) - fspecial('gaussian', 101, 21), 'symmetric');%vol(chEduDoG, 0, 500, 'hot')
    EduMask = EduDoG > 600; %vol(NucleiMask)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));
    EduMask= EduMask & NucDil;
    EduMask = imclose(squeeze(EduMask), strel('disk', 5));
    EduMask = bwareaopen(EduMask, 300);%vol(EduMask)
    EduAndSox2 = EduMask & Sox2Mask;

    %% Ki67
     for p=1:size(ch4, 3)
        %ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
        %ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [7,1], 0);
        ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [3,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch4_FT * 1000, 0, 100, hot)

    Ki67Mask = ch4_FT > 0.01;
    Ki67Mask= Ki67Mask & NucDil;
    Ki67Mask = bwareaopen(Ki67Mask, 50);%vol(Sox2Mask)
    Ki67AndSox2 = Ki67Mask & Sox2Mask;
    Ki67AndEduAndSox2 = Ki67Mask & Sox2Mask & EduMask;
    %Ki67Mask = bwareaopen(Ki67Mask, 50);%vol(Ki67Mask)

 clear('ch4_FT') 
    %% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
    Objects.Barcode = {MetaTable.Barcode};
    Objects.Batch = {MetaTable.Batch};
    Objects.Day = {MetaTable.Day};
    Objects.EduMask = sum(EduMask(:));
    Objects.TotalNucMask = sum(NucleiMask(:));
	Objects.NucDeadMask = sum(NucMaskHigh(:));
    Objects.Ki67Mask = sum(Ki67Mask(:));
    Objects.Sox2Mask = sum(Sox2Mask(:));
    Objects.Sox2MFI = mean(ch2(Sox2Mask));
	Objects.Sox2ByNucAlive = sum(Sox2Mask(:)) / sum(NucMaskAlive(:));
    Objects.Ki67ByNucAlive = sum(Ki67Mask(:)) / sum(NucMaskAlive(:));
    Objects.EduByNucAlive = sum(EduMask(:)) / sum(NucMaskAlive(:));
    Objects.EduByNuc = sum(EduMask(:)) / sum(NucleiMask(:));
	Objects.EduAndSox2BySox2 = sum(EduAndSox2(:)) / sum(Sox2Mask(:));
    Objects.Ki67AndSox2BySox2 = sum(Ki67AndSox2(:)) / sum(Sox2Mask(:));
    Objects.Ki67AndEduAndSox2BySox2 = sum(Ki67AndSox2(:)) / sum(Sox2Mask(:));

    %% Previews 
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');

    PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0 ; 0.1]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
    PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]); %imtool(PreviewHoechst)
    PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0 ; 0.1]));
    PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)
    
    PreviewNucDead = imoverlay2(imadjust(max(ch1,[],3),[0 ; 0.1]), bwperim(max(NucMaskHigh,[],3)), [0 0 1]);
    PreviewNucDead = imoverlay2(PreviewNucDead, BarMask, [1 1 1]); %imtool(PreviewNucDead)
    PreviewNucDeadMask = imoverlay2(imadjust(max(ch1,[],3),[0 ; 0.1]), max(NucMaskHigh,[],3), [0 0 1]); %imtool(PreviewNucDeadMask)
    PreviewNucAlive = imoverlay2(imadjust(max(ch1,[],3),[0 ; 0.1]), bwperim(max(NucMaskAlive,[],3)), [0 0 1]);
    PreviewNucAlive = imoverlay2(PreviewNucAlive, BarMask, [1 1 1]); %imtool(PreviewNucAlive)
    PreviewNucAliveMask = imoverlay2(imadjust(max(ch1,[],3),[0 ; 0.1]), max(NucMaskAlive,[],3), [0 0 1]); %imtool(PreviewNucAliveMask)
    
    PreviewSox2 = imoverlay2(imadjust(max(ch2,[],3),[0.008 ; 0.08]), bwperim(max(Sox2Mask,[],3)), [1 1 0]);
    PreviewSox2 = imoverlay2(PreviewSox2, BarMask, [1 1 1]); %imtool(PreviewSox2)
    PreviewSox2Mask = imoverlay2(imadjust(max(ch2,[],3),[0.008 ; 0.08]), max(Sox2Mask,[],3), [1 1 0]); %imtool(PreviewSox2Mask)
    PreviewSox2Raw = cat(3, imadjust(max(ch2, [], 3),[0.008 ; 0.08]), imadjust(max(ch2, [], 3),[0.008 ; 0.08]),max(chEmpty,[],3)); 
    PreviewSox2Raw = imoverlay2(PreviewSox2Raw, BarMask, [1 1 1]); %imtool(PreviewSox2Raw)
    
    PreviewEdu = imoverlay2(imadjust(max(ch3,[],3),[0.001 ; 0.1 ]), bwperim(max(EduMask,[],3)), [1 0 0]);
    PreviewEdu = imoverlay2(PreviewEdu, BarMask, [1 1 1]); %imtool(PreviewEdu)
    PreviewEduMask = imoverlay2(imadjust(max(ch3,[],3),[0.001 ; 0.2]), max(EduMask,[],3), [1 0 0]); %imtool(PreviewEduMask)
    PreviewEduRaw = cat(3, imadjust(max(ch3, [], 3),[0.001 ; 0.2]),max(chEmpty,[],3), max(chEmpty,[],3)); 
    PreviewEduRaw = imoverlay2(PreviewEduRaw, BarMask, [1 1 1]); %imtool(PreviewEduRaw)
    
    PreviewKi67 = imoverlay2(imadjust(max(ch4,[],3),[0.008 ; 0.08]), bwperim(max(Ki67Mask,[],3)), [0 1 0]);
    PreviewKi67 = imoverlay2(PreviewKi67, BarMask, [1 1 1]); %imtool(PreviewKi67)
    PreviewKi67Mask = imoverlay2(imadjust(max(ch4,[],3),[0.008 ; 0.08]), max(Ki67Mask,[],3), [0 1 0]); %imtool(PreviewKi67Mask)
    PreviewKi67Raw = cat(3,max(chEmpty,[],3),imadjust(max(ch4, [], 3),[0.008 ; 0.08]),max(chEmpty,[],3));
    PreviewKi67Raw  = imoverlay2(PreviewKi67Raw , BarMask, [1 1 1]); %imtool(PreviewKi67Raw)
    

    RGB_Sox2_Ki67 = cat (3, imadjust(max(ch2,[],3),[0.008 ; 0.08 ]), imadjust(max(ch4,[],3),[0.008 ; 0.08]),max(chEmpty,[],3));
    RGB_Sox2_Ki67  = imoverlay2(RGB_Sox2_Ki67, BarMask, [1 1 1]); %imtool(RGB_Ki67_Hoechst)
    
    RGB_Sox2_Edu = cat (3, imadjust(max(ch2,[],3),[0.008 ; 0.08 ]), imadjust(max(ch3,[],3),[0.001 ; 0.2]),max(chEmpty,[],3));
    RGB_Sox2_Edu   = imoverlay2(RGB_Sox2_Edu , BarMask, [1 1 1]); %imtool(RGB_Ki67_Hoechst)
    
    RGB_Sox2_Ki67_Edu = cat (3, imadjust(max(ch2,[],3),[0.008 ; 0.08 ]), imadjust(max(ch4,[],3),[0.008 ; 0.08]),imadjust(max(ch3,[],3),[0.001 ; 0.2]));
    RGB_Sox2_Ki67_Edu = imoverlay2(RGB_Sox2_Ki67_Edu, BarMask, [1 1 1]); %imtool(RGB_Ki67_Hoechst)
    
    
  
    %% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '_', Objects.AreaName{:}{:}, '_']
    imwrite(PreviewHoechst, [PreviewPath, filesep, IdentityString, 'PreviewHoechst.png'])
    imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
    imwrite(PreviewNucDead, [PreviewPath, filesep, IdentityString, 'PreviewNucDead.png'])
    imwrite(PreviewNucAliveMask, [PreviewPath, filesep, IdentityString, 'PreviewNucAliveMask.png'])
    imwrite(PreviewNucDeadMask, [PreviewPath, filesep, IdentityString, 'PreviewNucDeadMask.png'])
    
    imwrite(PreviewKi67, [PreviewPath, filesep, IdentityString, 'PreviewKi67.png'])
    imwrite(PreviewKi67Mask, [PreviewPath, filesep, IdentityString, 'PreviewKi67Mask.png'])
    imwrite(PreviewKi67Raw, [PreviewPath, filesep, IdentityString, 'PreviewKi67Raw.png'])
    
    imwrite(PreviewSox2, [PreviewPath, filesep, IdentityString, 'PreviewSox2.png'])
    imwrite(PreviewSox2Mask, [PreviewPath, filesep, IdentityString, 'PreviewSox2Mask.png'])
    imwrite(PreviewSox2Raw, [PreviewPath, filesep, IdentityString, 'PreviewSox2Raw.png'])
    
    imwrite(PreviewEdu, [PreviewPath, filesep, IdentityString, 'PreviewEdu.png'])
    imwrite(PreviewEduMask, [PreviewPath, filesep, IdentityString, 'PreviewEduMask.png'])
    imwrite(PreviewEduRaw, [PreviewPath, filesep, IdentityString, 'PreviewEduRaw.png'])
    
    imwrite(RGB_Sox2_Ki67, [PreviewPath, filesep, IdentityString, 'RGB_Sox2_Ki67.png'])
    imwrite(RGB_Sox2_Edu, [PreviewPath, filesep, IdentityString, 'RGB_Sox2_Edu.png'])
    imwrite(RGB_Sox2_Ki67_Edu, [PreviewPath, filesep, IdentityString, 'RGB_Sox2_Ki67_Edu.png'])
end

    
