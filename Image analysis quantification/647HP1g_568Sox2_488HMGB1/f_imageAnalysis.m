function  [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, ch4, PreviewPath, MetaTable)
    %vol(ch1, 0, 5000) Hoechst
    %vol(ch2, 0, 4000) 647 HPIg
    %vol(ch3, 0, 5000) 568 Sox2 (270 Mouse)
    %vol(ch4, 0, 5000) 488 HMGB1
    ObjectsThisOrganoid = table();
    %% Max projections
    %NucMax = max(ch1, [],3); imtool(NucMax,[])
    
    %% Segment Nuclei
%     NucDoG = imfilter(ch1, fspecial('gaussian', 21, 1) - fspecial('gaussian', 21, 3), 'symmetric'); % vol(NucDoG, 0, 100, 'hot')
%     NucMask = NucDoG > 100; %vol(NucMask, 0,1)

    ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 25, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 25, 5), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %figure; surf(fspecial('gaussian', 10, 5))
    ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch1DoG, 0, 2000, 'hot')
    NucleiMask = ch1DoG > 50; %vol(NucleiMask)
    %NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    NucleiMask = bwareaopen(NucleiMask, 50);%vol(NucleiMask)
    ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch3LP, 0, 3000, 'hot')
    NucMaskHigh =  (ch1LP > 2000) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucMaskAlive)
    %NucMask = ch1 > 3000; vol(NucMask, 0,1)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));

 

     %% Sox2
    Sox2DoG = imfilter(double(ch3), fspecial('gaussian', 101, 1) - fspecial('gaussian', 101, 21), 'symmetric');%vol(chSox2DoG, 0, 500, 'hot')
    Sox2Mask = Sox2DoG > 100; %vol(NucleiMask)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));
    Sox2Mask= Sox2Mask & NucDil;
    Sox2Mask = imclose(squeeze(Sox2Mask), strel('disk', 5));
    Sox2Mask = bwareaopen(Sox2Mask, 1000);%vol(Sox2Mask)
        %% HP1g

    HP1gDoG = imfilter(double(ch2), fspecial('gaussian', 101, 1) - fspecial('gaussian', 101, 21), 'symmetric');%vol(chSox2DoG, 0, 500, 'hot')
    HP1gMask = HP1gDoG > 50; %vol(NucleiMask)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));
    HP1gMask= HP1gMask & NucDil;
    HP1gMask = imclose(squeeze(HP1gMask), strel('disk', 5));
    HP1gMask = bwareaopen(HP1gMask, 300);%vol(Sox2Mask)
    HP1gHighMask = (HP1gDoG > 500) .* HP1gMask;
    HP1gAndSox2 = HP1gMask & Sox2Mask;
    HP1gHighAndSox2 = HP1gHighMask  & Sox2Mask;
 clear('ch2_FT')  
    %% HMGB1
    HMGB1DoG = imfilter(double(ch4), fspecial('gaussian', 101, 1) - fspecial('gaussian', 101, 21), 'symmetric');%vol(chSox2DoG, 0, 500, 'hot')
    HMGB1Mask = HMGB1DoG > 100; %vol(NucleiMask)
    %HMGB1Mask = bwareaopen(HMGB1Mask, 50);
    HMGB1NucMask= HMGB1Mask & NucleiMask;
    HMGB1cytoMask = HMGB1Mask & ~HMGB1NucMask;
    %NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));

 clear('ch4DoG') 
     %% HMGB1 and Sox2
     HMGB1AndSox2= Sox2Mask & HMGB1Mask;
     HMGB1NucAndSox2 = Sox2Mask & HMGB1NucMask;
     HMGB1CytoAndSox2 = Sox2Mask & HMGB1cytoMask;
     
    %% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
    Objects.Barcode = {MetaTable.Barcode};
    Objects.Batch = {MetaTable.Batch};
    Objects.Day = {MetaTable.Day};
    Objects.Sox2Mask = sum(Sox2Mask(:));
    Objects.TotalNucMask = sum(NucleiMask(:));
	Objects.NucDeadMask = sum(NucMaskHigh(:));
	Objects.Sox2MFI = mean(ch3(Sox2Mask));
    Objects.HMGB1Mask = sum(HMGB1Mask(:));
    Objects.HMGB1CytoMFI = mean(ch4(HMGB1cytoMask));
    Objects.HMGB1NucMFI = mean(ch4(HMGB1NucMask));
    Objects.HMGB1NucMask = sum(HMGB1NucMask(:));
    Objects.HMGB1cytoMask = sum(HMGB1cytoMask(:));
    Objects.HP1gMask = sum(HP1gMask(:));
    Objects.HP1gHighMask = sum(HP1gHighMask(:));
    Objects.HP1gMFI = mean(ch2(HP1gAndSox2));
	Objects.HP1gHighMFIinSox2 = mean(ch2(HP1gHighAndSox2));
	Objects.HP1gByNucAlive = sum(HP1gMask(:)) / sum(NucMaskAlive(:));
    Objects.HP1gHighByNucAlive = sum(HP1gHighMask(:)) / sum(NucMaskAlive(:));
    Objects.HMGB1ByNucAlive = sum(HMGB1Mask(:)) / sum(NucMaskAlive(:));
    Objects.HMGB1CytoByNucAlive = sum(HMGB1cytoMask(:)) / sum(NucMaskAlive(:));
    Objects.HMGB1NucByNucAlive = sum(HMGB1NucMask(:)) / sum(NucMaskAlive(:));
    Objects.Sox2ByNucAlive = sum(Sox2Mask(:)) / sum(NucMaskAlive(:));
    
    
	Objects.HP1gandSox2BySox2 = sum(HP1gAndSox2(:));
    Objects.HP1gHighandSox2BySox2 = sum(HP1gHighAndSox2(:)) / sum(Sox2Mask(:));
 	Objects.HMGB1andSox2BySox2 = sum(HMGB1AndSox2(:)) / sum(Sox2Mask(:));
    Objects.HMGB1NucAndSox2 = sum(HMGB1NucAndSox2(:)) / sum(Sox2Mask(:));
    Objects.HMGB1CytoAndSox2 = sum(HMGB1CytoAndSox2(:)) / sum(Sox2Mask(:));
    
    %% Previews 
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');

    PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0 ; 0.1]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
    PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]); %imtool(PreviewHoechst)
    PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0 ; 0.1]));
    PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)
    
    PreviewNucDead = imoverlay2(imadjust(max(ch1,[],3),[0 ; 0.2]), bwperim(max(NucMaskHigh,[],3)), [0 0 1]);
    PreviewNucDead = imoverlay2(PreviewNucDead, BarMask, [1 1 1]); %imtool(PreviewNucDead)
    PreviewNucDeadMask = imoverlay2(imadjust(max(ch1,[],3),[0 ; 0.2]), max(NucMaskHigh,[],3), [0 0 1]); %imtool(PreviewNucDeadMask)
    PreviewNucAlive = imoverlay2(imadjust(max(ch1,[],3),[0 ; 0.2]), bwperim(max(NucMaskAlive,[],3)), [0 0 1]);
    PreviewNucAlive = imoverlay2(PreviewNucAlive, BarMask, [1 1 1]); %imtool(PreviewNucAlive)
    PreviewNucAliveMask = imoverlay2(imadjust(max(ch1,[],3),[0 ; 0.2]), max(NucMaskAlive,[],3), [0 0 1]); %imtool(PreviewNucAliveMask)
    
    PreviewHP1g = imoverlay2(imadjust(max(ch2,[],3),[0.001 ; 0.007]), bwperim(max(HP1gMask,[],3)), [1 1 0]);
    PreviewHP1g = imoverlay2(PreviewHP1g, BarMask, [1 1 1]); %imtool(PreviewHP1g)
    PreviewHP1gHigh = imoverlay2(imadjust(max(ch2,[],3),[0.001 ; 0.007]), bwperim(max(HP1gHighMask,[],3)), [1 1 0]); %imtool(PreviewHP1gHigh)
    PreviewHP1gMask = imoverlay2(imadjust(max(ch2,[],3),[0.001 ; 0.007]), max(HP1gMask,[],3), [1 1 0]); %imtool(PreviewHP1gMask)
    PreviewHP1gRaw = cat(3, imadjust(max(ch2, [], 3),[0.001 ; 0.007]), imadjust(max(ch2, [], 3),[0.01 ; 0.2]),max(chEmpty,[],3)); 
    PreviewHP1gRaw = imoverlay2(PreviewHP1gRaw, BarMask, [1 1 1]); %imtool(PreviewHP1gRaw)
    
    PreviewSox2 = imoverlay2(imadjust(max(ch3,[],3),[0.008 ; 0.07 ]), bwperim(max(Sox2Mask,[],3)), [1 0 0]);
    PreviewSox2 = imoverlay2(PreviewSox2, BarMask, [1 1 1]); %imtool(PreviewSox2)
    PreviewSox2Mask = imoverlay2(imadjust(max(ch3,[],3),[0.008 ; 0.07]), max(Sox2Mask,[],3), [1 0 0]); %imtool(PreviewSox2Mask)
    PreviewSox2Raw = cat(3, imadjust(max(ch3, [], 3),[0.008 ; 0.07]),max(chEmpty,[],3), max(chEmpty,[],3)); 
    PreviewSox2Raw = imoverlay2(PreviewSox2Raw, BarMask, [1 1 1]); %imtool(PreviewSox2Raw)
    
    
    PreviewHMGB1 = imoverlay2(imadjust(max(ch4,[],3),[0.005 ; 0.01]), bwperim(max(HMGB1Mask,[],3)), [0 1 0]);
    PreviewHMGB1 = imoverlay2(PreviewHMGB1, BarMask, [1 1 1]); %imtool(PreviewHMGB1)
    PreviewHMGB1Mask = imoverlay2(imadjust(max(ch4,[],3),[0.008 ; 0.03]), max(HMGB1Mask,[],3), [0 1 0]); %imtool(PreviewHMGB1Mask)
    PreviewHMGB1Raw = cat(3,max(chEmpty,[],3),imadjust(max(ch4, [], 3),[0.008 ; 0.03]),max(chEmpty,[],3));
    PreviewHMGB1Raw  = imoverlay2(PreviewHMGB1Raw , BarMask, [1 1 1]); %imtool(PreviewHMGB1Raw)
    

    RGB_HMGB1_Sox2 = cat (3, imadjust(max(ch3,[],3),[0.008 ; 0.07 ]), imadjust(max(ch4,[],3),[0.008 ; 0.03]),max(chEmpty,[],3));
    RGB_HMGB1_Sox2  = imoverlay2(RGB_HMGB1_Sox2, BarMask, [1 1 1]); %imtool(RGB_HMGB1_Hoechst)
    
    RGB_HP1g_Sox2 = cat (3, imadjust(max(ch3,[],3),[0.008 ; 0.07 ]), imadjust(max(ch2,[],3),[0.001 ; 0.007]),max(chEmpty,[],3));
    RGB_HP1g_Sox2   = imoverlay2(RGB_HP1g_Sox2 , BarMask, [1 1 1]); %imtool(RGB_HMGB1_Hoechst)
    
    
  
    %% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '__', Objects.AreaName{:}{:}, '_']
    imwrite(PreviewHoechst, [PreviewPath, filesep, IdentityString, 'PreviewHoechst.png'])
    imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
    imwrite(PreviewNucDead, [PreviewPath, filesep, IdentityString, 'PreviewNucDead.png'])
    imwrite(PreviewNucAliveMask, [PreviewPath, filesep, IdentityString, 'PreviewNucAliveMask.png'])
    imwrite(PreviewNucDeadMask, [PreviewPath, filesep, IdentityString, 'PreviewNucDeadMask.png'])
    
    imwrite(PreviewHMGB1, [PreviewPath, filesep, IdentityString, 'PreviewHMGB1.png'])
    imwrite(PreviewHMGB1Mask, [PreviewPath, filesep, IdentityString, 'PreviewHMGB1Mask.png'])
    imwrite(PreviewHMGB1Raw, [PreviewPath, filesep, IdentityString, 'PreviewHMGB1Raw.png'])
    
    imwrite(PreviewHP1g, [PreviewPath, filesep, IdentityString, 'PreviewHP1g.png'])
    imwrite(PreviewHP1gMask, [PreviewPath, filesep, IdentityString, 'PreviewHP1gMask.png'])
    imwrite(PreviewHP1gRaw, [PreviewPath, filesep, IdentityString, 'PreviewHP1gRaw.png'])
    
    imwrite(PreviewSox2, [PreviewPath, filesep, IdentityString, 'PreviewSox2.png'])
    imwrite(PreviewSox2Mask, [PreviewPath, filesep, IdentityString, 'PreviewSox2Mask.png'])
    imwrite(PreviewSox2Raw, [PreviewPath, filesep, IdentityString, 'PreviewSox2Raw.png'])
    
    imwrite(RGB_HP1g_Sox2, [PreviewPath, filesep, IdentityString, 'RGB_HP1g_Sox2.png'])
    imwrite(RGB_HMGB1_Sox2, [PreviewPath, filesep, IdentityString, 'RGB_HMGB1_Sox2.png'])
end

    
