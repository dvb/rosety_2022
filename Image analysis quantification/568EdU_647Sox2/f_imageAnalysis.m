function  [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, PreviewPath, MetaTable)
    %vol(ch1, 0, 5000) Hoechst
    %vol(ch2, 0, 4000) 647 Sox2
    %vol(ch3, 0, 5000) 568 Edu 
    %vol(ch4, 0, 5000) 488 Ki67
    ObjectsThisOrganoid = table();
    %% Max projections
    %NucMax = max(ch1, [],3); imtool(NucMax,[])
    
    %% Segment Nuclei
%     NucDoG = imfilter(ch1, fspecial('gaussian', 21, 1) - fspecial('gaussian', 21, 3), 'symmetric'); % vol(NucDoG, 0, 100, 'hot')
%     NucMask = NucDoG > 100; %vol(NucMask, 0,1)

    ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 25, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 25, 5), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %figure; surf(fspecial('gaussian', 10, 5))
    ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch1DoG, 0, 2000, 'hot')
    NucleiMask = ch1DoG > 30; %vol(NucleiMask)
    NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    NucleiMask = imclose(squeeze(NucleiMask), strel('disk', 5));
    NucleiMask = bwareaopen(NucleiMask, 50);%vol(NucleiMask)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));
    %ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch3LP, 0, 3000, 'hot')
    NucMaskHigh =  (ch1DoG > 100) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    NucMaskHigh = imclose(squeeze(NucMaskHigh), strel('disk', 5));
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucMaskAlive)
    %NucMask = ch1 > 3000; vol(NucMask, 0,1)
   
    %% Sox2
    Sox2DoG = imfilter(double(ch2), fspecial('gaussian', 101, 1) - fspecial('gaussian', 101, 21), 'symmetric');%vol(chEduDoG, 0, 500, 'hot')
    Sox2Mask = Sox2DoG > 40; %vol(NucleiMask)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));
    Sox2Mask= Sox2Mask & NucDil;
    Sox2Mask = imclose(squeeze(Sox2Mask), strel('disk', 5));
    Sox2Mask = bwareaopen(Sox2Mask, 1200);
    
    %% Edu
    EduDoG = imfilter(double(ch3), fspecial('gaussian', 101, 1) - fspecial('gaussian', 101, 21), 'symmetric');%vol(chEduDoG, 0, 500, 'hot')
    EduMask = EduDoG > 400; %vol(NucleiMask)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));
    EduMask= EduMask & NucDil;
    EduMask = imclose(squeeze(EduMask), strel('disk', 5));
    EduMask = bwareaopen(EduMask, 300);%vol(EduMask)
    EduAndSox2 = EduMask & Sox2Mask;

    
    %% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
    Objects.Barcode = {MetaTable.Barcode};
    Objects.Batch = {MetaTable.Batch};
    Objects.Day = {MetaTable.Day};
    Objects.EduMask = sum(EduMask(:));
    Objects.EduInSox2MFI = mean(ch3(EduAndSox2));
    Objects.TotalNucMask = sum(NucleiMask(:));
	Objects.NucDeadMask = sum(NucMaskHigh(:));
    
    Objects.Sox2Mask = sum(Sox2Mask(:));
    Objects.Sox2MFI = mean(ch2(Sox2Mask));
	Objects.Sox2ByNucAlive = sum(Sox2Mask(:)) / sum(NucMaskAlive(:));
    
    Objects.EduByNucAlive = sum(EduMask(:)) / sum(NucMaskAlive(:));
    Objects.EduByNuc = sum(EduMask(:)) / sum(NucleiMask(:));
	Objects.EduAndSox2BySox2 = sum(EduAndSox2(:)) / sum(Sox2Mask(:));
  
    %% Previews 
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');
 
    PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0.002 ; 0.015]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
    PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]); %imtool(PreviewHoechst)
    PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0.002 ; 0.015]));
    PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)
    
    PreviewNucDead = imoverlay2(imadjust(max(ch1,[],3),[0.002 ; 0.015]), bwperim(max(NucMaskHigh,[],3)), [0 0 1]);
    PreviewNucDead = imoverlay2(PreviewNucDead, BarMask, [1 1 1]); %imtool(PreviewNucDead)
    PreviewNucDeadMask = imoverlay2(imadjust(max(ch1,[],3),[0.002 ; 0.015]), max(NucMaskHigh,[],3), [0 0 1]); %imtool(PreviewNucDeadMask)
    PreviewNucAlive = imoverlay2(imadjust(max(ch1,[],3),[0.002 ; 0.015]), bwperim(max(NucMaskAlive,[],3)), [0 0 1]);
    PreviewNucAlive = imoverlay2(PreviewNucAlive, BarMask, [1 1 1]); %imtool(PreviewNucAlive)
    PreviewNucAliveMask = imoverlay2(imadjust(max(ch1,[],3),[0.002 ; 0.015]), max(NucMaskAlive,[],3), [0 0 1]); %imtool(PreviewNucAliveMask)
    
    PreviewSox2 = imoverlay2(imadjust(max(ch2,[],3),[0.0015 ; 0.01]), bwperim(max(Sox2Mask,[],3)), [1 1 0]);
    PreviewSox2 = imoverlay2(PreviewSox2, BarMask, [1 1 1]); %imtool(PreviewSox2)
    PreviewSox2Mask = imoverlay2(imadjust(max(ch2,[],3),[0.0015 ; 0.01]), max(Sox2Mask,[],3), [1 1 0]); %imtool(PreviewSox2Mask)
    PreviewSox2Raw = cat(3, imadjust(max(ch2, [], 3),[0.001 ; 0.02]), imadjust(max(ch2, [], 3),[0.001 ; 0.08]),max(chEmpty,[],3)); 
    PreviewSox2Raw = imoverlay2(PreviewSox2Raw, BarMask, [1 1 1]); %imtool(PreviewSox2Raw)
    PreviewSox2RawRed = cat(3, imadjust(max(ch2, [], 3),[0.001 ; 0.015]), max(chEmpty,[],3),max(chEmpty,[],3)); 
    PreviewSox2RawRed = imoverlay2(PreviewSox2RawRed, BarMask, [1 1 1]); %imtool(PreviewSox2RawRed)

    
    
    PreviewEdu = imoverlay2(imadjust(max(ch3,[],3),[0.001 ; 0.05 ]), bwperim(max(EduMask,[],3)), [0 1 0]);
    PreviewEdu = imoverlay2(PreviewEdu, BarMask, [1 1 1]); %imtool(PreviewEdu)
    PreviewEduMask = imoverlay2(imadjust(max(ch3,[],3),[0.001 ; 0.05]), max(EduMask,[],3), [0 1 0]); %imtool(PreviewEduMask)
    PreviewEduRaw = cat(3, max(chEmpty,[],3),imadjust(max(ch3, [], 3),[0.001 ; 0.05]), max(chEmpty,[],3)); 
    PreviewEduRaw = imoverlay2(PreviewEduRaw, BarMask, [1 1 1]); %imtool(PreviewEduRaw)
    

    
    RGB_Sox2_Edu = cat (3, imadjust(max(ch2,[],3),[0.001 ; 0.015]), imadjust(max(ch3,[],3),[0.001 ; 0.05]),max(chEmpty,[],3));
    RGB_Sox2_Edu   = imoverlay2(RGB_Sox2_Edu , BarMask, [1 1 1]); %imtool(RGB_Ki67_Hoechst)
    
    RGB_Sox2_Hoechst = cat (3, imadjust(max(ch2,[],3),[0.001 ; 0.015]),max(chEmpty,[],3),imadjust(max(ch1, [], 3),[0.002 ; 0.015]));
    RGB_Sox2_Hoechst   = imoverlay2(RGB_Sox2_Hoechst  , BarMask, [1 1 1]); %imtool(RGB_Sox2_Hoechst)
  
  
    %% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '_', Objects.AreaName{:}{:}, '_']
    imwrite(PreviewHoechst, [PreviewPath, filesep, IdentityString, 'PreviewHoechst.png'])
    imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
    imwrite(PreviewNucDead, [PreviewPath, filesep, IdentityString, 'PreviewNucDead.png'])
    imwrite(PreviewNucAliveMask, [PreviewPath, filesep, IdentityString, 'PreviewNucAliveMask.png'])
    imwrite(PreviewNucDeadMask, [PreviewPath, filesep, IdentityString, 'PreviewNucDeadMask.png'])
    
    
    imwrite(PreviewSox2, [PreviewPath, filesep, IdentityString, 'PreviewSox2.png'])
    imwrite(PreviewSox2Mask, [PreviewPath, filesep, IdentityString, 'PreviewSox2Mask.png'])
    imwrite(PreviewSox2Raw, [PreviewPath, filesep, IdentityString, 'PreviewSox2Raw.png'])
    imwrite(PreviewSox2RawRed, [PreviewPath, filesep, IdentityString, 'PreviewSox2RawRed.png'])
    
    imwrite(PreviewEdu, [PreviewPath, filesep, IdentityString, 'PreviewEdu.png'])
    imwrite(PreviewEduMask, [PreviewPath, filesep, IdentityString, 'PreviewEduMask.png'])
    imwrite(PreviewEduRaw, [PreviewPath, filesep, IdentityString, 'PreviewEduRaw.png'])
    
    imwrite(RGB_Sox2_Edu, [PreviewPath, filesep, IdentityString, 'RGB_Sox2_Edu.png'])
    imwrite(RGB_Sox2_Hoechst, [PreviewPath, filesep, IdentityString, 'RGB_Sox2_Hoechst.png'])
end

    
