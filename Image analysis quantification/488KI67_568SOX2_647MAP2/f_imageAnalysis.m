function  [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, ch4, PreviewPath, MetaTable)
    %vol(ch1, 0, 5000) Hoechst
    %vol(ch2, 0, 4000) 647 MAP2
    %vol(ch3, 0, 5000) 568 Sox2
    %vol(ch4, 0, 5000) 488 Ki67

    ObjectsThisOrganoid = table();
    
    %% Max projections
    %NucMax = max(ch1, [],3); imtool(NucMax,[])
    
    %% Segment Nuclei
%     NucDoG = imfilter(ch1, fspecial('gaussian', 21, 3) - fspecial('gaussian', 21, 5), 'symmetric'); % vol(NucDoG, 0, 100, 'hot')
%     NucMask = NucDoG > 100; %vol(NucMask, 0,1)
    ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 21, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 21, 3), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch3DoG, 0, 2000, 'hot')
    NucleiMask = ch1DoG > 40; %vol(NucleiMask)
    NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 2)), strel('sphere',1));
    NucleiMask = bwareaopen(NucleiMask, 300);%vol(NucleiMask)
    ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch3LP, 0, 3000, 'hot')
    NucMaskHigh =  (ch1LP > 2000) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucMaskAlive)
 
    
    %NucMask = ch1 > 3000; vol(NucMask, 0,1)


    
    %% Sox2
    %ch3BlurSmall = imfilter(double(ch3), fspecial('gaussian', 21, 1), 'symmetric');%vol(ch3BlurSmall)
    %ch3BlurBig = imfilter(double(ch3), fspecial('gaussian', 21, 3), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %ch3BlurBig = imfilter(double(ch3), fspecial('gaussian', 55, 3), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %ch3DoG = ch3BlurSmall - ch3BlurBig; %vol(ch3DoG, 0, 2000, 'hot')
    Sox2DoG = imfilter(double(ch3), fspecial('gaussian', 55, 1)-fspecial('gaussian',55,11), 'symmetric');%vol(ch3BlurSmall)    ch3BlurBig = imfilter(double(ch3), fspecial('gaussian', 55, 3), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    Sox2Mask = Sox2DoG > 100; %vol(NucleiMask)
    %Sox2Mask = imdilate(imdilate(Sox2Mask, strel('disk', 1)),strel('sphere',1));
    Sox2Mask= Sox2Mask & NucleiMask;
    Sox2Mask = imclose(squeeze(Sox2Mask), strel('disk', 5));
    Sox2Mask = bwareaopen(Sox2Mask, 500);%vol(Sox2Mask)
    %% MAP2

    for p=1:size(ch2, 3)
        ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [15,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [7,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch2_FT * 1000, 0, 100, hot)

    MAP2Mask = ch2_FT > 0.008;
    MAP2Mask = bwareaopen(MAP2Mask, 100);
 clear('ch2_FT') 
    %% Ki67
    
    for p=1:size(ch4, 3)
        %ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
        ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch4_FT * 1000, 0, 100, hot)

    Ki67Mask = ch4_FT > 0.01;
    Ki67Mask= Ki67Mask & NucleiMask;
    Ki67Mask = bwareaopen(Ki67Mask, 50);%vol(Sox2Mask)
    
    %Ki67Mask = bwareaopen(Ki67Mask, 50);%vol(Ki67Mask)

 clear('ch4_FT') 

    
    %% Feature extraction
    Objects = table();
    Objects.OR_Idx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
    Objects.Barcode = {MetaTable.Barcode};
    %Objects.Batch = {MetaTable.Batch};
    Objects.Day = {MetaTable.Day};
    Objects.Sox2Mask = sum(Sox2Mask(:));
    Objects.Sox2MFI = mean(ch3(Sox2Mask));
    Objects.MAP2Mask = sum(MAP2Mask(:));
    Objects.Ki67Mask = sum(MAP2Mask(:));
    Objects.TotalNucMask = sum(NucleiMask(:));
	Objects.NucDeadMask = sum(NucMaskHigh(:));
	Objects.MAP2ByNucAlive = sum(MAP2Mask(:)) / sum(NucMaskAlive(:));
	Objects.Ki67ByNucAlive = sum(Ki67Mask(:)) / sum(NucMaskAlive(:));
    Objects.Sox2ByNucAlive = sum(Sox2Mask(:)) / sum(NucMaskAlive(:));
    
    %% Previews 
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');

    %RGB = cat(3, imadjust(max(chVimentin, [], 3), [0 0.3], [0 1]), imadjust(max(ch3, [], 3), [0 0.99], [0 1]), imadjust(max(ch1, [], 3), [0 0.3], [0 1]));
    %imtool(RGB)
    PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0 0.1]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
    PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]); %imtool(PreviewHoechst)
    PreviewHoechstMask = imoverlay2(imadjust(max(ch1,[],3),[0 0.1]), max(NucleiMask,[],3), [0 0 1]); %imtool(PreviewHoechstMask)
    PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0 ; 0.07]));
    PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)
    PreviewNucDead = imoverlay2(imadjust(max(ch1,[],3),[0 0.08]), bwperim(max(NucMaskHigh,[],3)), [0 0 1]);
    PreviewNucDead = imoverlay2(PreviewNucDead, BarMask, [1 1 1]); %imtool(PreviewNucDead)
    PreviewNucAlive = imoverlay2(imadjust(max(ch1,[],3),[0 0.08]), bwperim(max(NucMaskAlive,[],3)), [0 0 1]);
    PreviewNucAlive = imoverlay2(PreviewNucAlive, BarMask, [1 1 1]); %imtool(PreviewNucAlive)
    
    PreviewMAP2 = imoverlay2(imadjust(max(ch2,[],3),[0 0.04]), bwperim(max(MAP2Mask,[],3)), [1 0 0]);
    PreviewMAP2 = imoverlay2(PreviewMAP2, BarMask, [1 1 1]); %imtool(PreviewMAP2)
    PreviewMAP2Mask = imoverlay2(imadjust(max(ch2,[],3),[0 0.04]), max(MAP2Mask,[],3), [1 0 0]); %imtool(PreviewMAP2Mask)
    PreviewMAP2Raw = cat(3, imadjust(max(ch2, [], 3),[0.0001 ; 0.05]), max(chEmpty,[],3),max(chEmpty,[],3)); 
    PreviewMAP2Raw = imoverlay2(PreviewMAP2Raw, BarMask, [1 1 1]); %imtool(PreviewMAP2Raw)
    
    PreviewSox2 = imoverlay2(imadjust(max(ch3,[],3),[0 0.035]), bwperim(max(Sox2Mask,[],3)), [1 0 0]);
    PreviewSox2 = imoverlay2(PreviewSox2, BarMask, [1 1 1]); %imtool(PreviewSox2)
    PreviewSox2Mask = imoverlay2(imadjust(max(ch3,[],3),[0 0.035]), max(Sox2Mask,[],3), [0 1 0]); %imtool(PreviewSox2Mask)
    PreviewSox2Raw = cat(3, imadjust(max(ch3, [], 3),[0.0001 ; 0.03]),max(chEmpty,[],3), max(chEmpty,[],3)); 
    PreviewSox2Raw = imoverlay2(PreviewSox2Raw, BarMask, [1 1 1]); %imtool(PreviewSox2Raw)
    
    
    PreviewKi67 = imoverlay2(imadjust(max(ch4,[],3),[0 0.07]), bwperim(max(Ki67Mask,[],3)), [0 1 0]);
    PreviewKi67 = imoverlay2(PreviewKi67, BarMask, [1 1 1]); %imtool(PreviewKi67)
    PreviewKi67Mask = imoverlay2(imadjust(max(ch4,[],3),[0 0.07]), max(Ki67Mask,[],3), [0 1 0]); %imtool(PreviewKi67Mask)
    PreviewKi67Raw = cat(3,max(chEmpty,[],3),imadjust(max(ch4, [], 3),[0.0001 ; 0.065]),max(chEmpty,[],3));
    PreviewKi67Raw  = imoverlay2(PreviewKi67Raw , BarMask, [1 1 1]); %imtool(PreviewKi67Raw)
    

    RGB = cat (3,  imadjust(max(ch3, [], 3),[0.0001 ; 0.03]), imadjust(max(ch4, [], 3),[0.0001 ; 0.065]),max(chEmpty,[],3));
    RGB = imoverlay2(RGB, BarMask, [1 1 1]); %imtool(RGB)
    
    %% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '__', Objects.AreaName{:}{:}, '_']
    imwrite(PreviewHoechst, [PreviewPath, filesep, IdentityString, 'PreviewHoechst.png'])
    imwrite(PreviewHoechstMask, [PreviewPath, filesep, IdentityString, 'PreviewHoechstMask.png'])
    imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
    imwrite(PreviewNucDead, [PreviewPath, filesep, IdentityString, 'PreviewNucDead.png'])
    
    imwrite(PreviewMAP2, [PreviewPath, filesep, IdentityString, 'PreviewMAP2.png'])
    imwrite(PreviewMAP2Mask, [PreviewPath, filesep, IdentityString, 'PreviewMAP2Mask.png'])
    imwrite(PreviewMAP2Raw, [PreviewPath, filesep, IdentityString, 'PreviewMAP2Raw.png'])
    
    imwrite(PreviewKi67, [PreviewPath, filesep, IdentityString, 'PreviewKi67.png'])
    imwrite(PreviewKi67Mask, [PreviewPath, filesep, IdentityString, 'PreviewKi67Mask.png'])
    imwrite(PreviewKi67Raw, [PreviewPath, filesep, IdentityString, 'PreviewKi67Raw.png'])
    
    imwrite(PreviewSox2, [PreviewPath, filesep, IdentityString, 'PreviewSox2.png'])
    imwrite(PreviewSox2Mask, [PreviewPath, filesep, IdentityString, 'PreviewSox2Mask.png'])
    imwrite(PreviewSox2Raw, [PreviewPath, filesep, IdentityString, 'PreviewSox2Raw.png'])
    
    imwrite(RGB, [PreviewPath, filesep, IdentityString, 'RGB.png'])
end

    
