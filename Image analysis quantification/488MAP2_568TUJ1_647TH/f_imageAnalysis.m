function  [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, ch4, PreviewPath, MetaTable)
    %vol(ch1, 0, 5000) Hoechst
    %vol(ch2, 0, 4000) 647 TH
    %vol(ch3, 0, 5000) 568 Tuj1
    %vol(ch4, 0, 5000) 488 MAP2
    ObjectsThisOrganoid = table();
    %% Max projections
    %NucMax = max(ch1, [],3); imtool(NucMax,[])
    
    %% Segment Nuclei
%     NucDoG = imfilter(ch1, fspecial('gaussian', 21, 1) - fspecial('gaussian', 21, 3), 'symmetric'); % vol(NucDoG, 0, 100, 'hot')
%     NucMask = NucDoG > 100; %vol(NucMask, 0,1)

    ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 25, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 25, 5), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %figure; surf(fspecial('gaussian', 10, 5))
    ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch1DoG, 0, 2000, 'hot')
    NucleiMask = ch1DoG > 50; %vol(NucleiMask)
    %NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    NucleiMask = bwareaopen(NucleiMask, 50);%vol(NucleiMask)
    ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch3LP, 0, 3000, 'hot')
    NucMaskHigh =  (ch1LP > 2000) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucMaskAlive)
    %NucMask = ch1 > 3000; vol(NucMask, 0,1)
    
    %% Tuj1
    for p=1:size(ch3, 3)
        ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [15,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch4_FT * 1000, 0, 100, hot)

    Tuj1Mask = ch3_FT > 0.04;
    
    Tuj1Mask = bwareaopen(Tuj1Mask, 100);%vol(THMask)
    
 clear('ch3_FT')   

    %% MAP2
    
    for p=1:size(ch4, 3)
        ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch4_FT * 1000, 0, 100, hot)

    MAP2Mask = ch4_FT > 0.085;
    
    MAP2Mask = bwareaopen(MAP2Mask, 100);%vol(THMask)

    
 clear('ch4_FT') 
     %% TH

    for p=1:size(ch2, 3)
        ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [15,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [7,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch2_FT * 1000, 0, 100, hot)

    THMask = ch2_FT > 0.007;
    THMask = bwareaopen(THMask, 100);
    THandMAP2 = THMask & MAP2Mask;
    THandTuj1 = THMask & Tuj1Mask;
 clear('ch2_FT') 
 

    %% TH Fragmentation

    % Define structuring element for surface detection
    Conn6 = strel('sphere', 1); % 6 connectivity
    % Detect surface
    SurfaceTH = THMask & ~(imerode(THMask, Conn6));
    %vol(SurfaceTH)
    
    
    %% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
    Objects.Barcode = {MetaTable.Barcode};
    Objects.Batch = {MetaTable.Batch};
    Objects.Day = {MetaTable.Day};
    Objects.Tuj1Mask = sum(Tuj1Mask(:));
	Objects.Tuj1MFI = mean(ch3(Tuj1Mask));
    Objects.MAP2Mask = sum(MAP2Mask(:));
    Objects.THMask = sum(THMask(:));
    Objects.THMFI = mean(ch2(THMask));
    Objects.TotalNucMask = sum(NucleiMask(:));
	Objects.NucDeadMask = sum(NucMaskHigh(:));
	Objects.MAP2ByNucAlive = sum(MAP2Mask(:)) / sum(NucMaskAlive(:));
	Objects.THByNucAlive = sum(THMask(:)) / sum(NucMaskAlive(:));
	Objects.THandMAP2ByMAP2 = sum(THandMAP2(:)) / sum(MAP2Mask(:));
	Objects.THandMAP2ByNucAlive = sum(THandMAP2(:)) / sum(NucMaskAlive(:));
    Objects.THandTuj1ByTuj1 = sum(THandTuj1(:)) / sum(Tuj1Mask(:));
    Objects.THandTuj1ByNucAlive = sum(THandTuj1(:)) / sum(NucMaskAlive(:));
    Objects.Tuj1ByNucAlive = sum(Tuj1Mask(:)) / sum(NucMaskAlive(:));
    Objects.THFragmentation = sum(SurfaceTH(:)) / sum(THMask(:));
    
    
    
    %% Previews 
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');

    %RGB = cat(3, imadjust(max(chVimentin, [], 3), [0 0.3], [0 1]), imadjust(max(ch3, [], 3), [0 0.99], [0 1]), imadjust(max(ch1, [], 3), [0 0.3], [0 1]));
    %imtool(RGB)
     PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
     PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]); %imtool(PreviewHoechst)
     PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0.001 ; 0.07]));
     PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)

     PreviewNucDead = imoverlay2(imadjust(max(ch1,[],3),[0 0.1]), bwperim(max(NucMaskHigh,[],3)), [0 0 1]);
     PreviewNucDead = imoverlay2(PreviewNucDead, BarMask, [1 1 1]); %imtool(PreviewNucDead)
     PreviewNucDeadMask = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), max(NucMaskHigh,[],3), [0 0 1]); %imtool(PreviewNucDeadMask)
     PreviewNucAlive = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), bwperim(max(NucMaskAlive,[],3)), [0 0 1]);
     PreviewNucAlive = imoverlay2(PreviewNucAlive, BarMask, [1 1 1]); %imtool(PreviewNucAlive)
     PreviewNucAliveMask = imoverlay2(imadjust(max(ch1,[],3),[0 0.1]), max(NucMaskAlive,[],3), [0 0 1]); %imtool(PreviewNucAliveMask)
     
    PreviewTH = imoverlay2(imadjust(max(ch2,[],3),[0.0015 ; 0.04]), bwperim(max(THMask,[],3)), [1 1 0]);
    PreviewTH = imoverlay2(PreviewTH, BarMask, [1 1 1]); %imtool(PreviewTH)
    PreviewTHMask = imoverlay2(imadjust(max(ch2,[],3),[0.0015 ; 0.06]), max(THMask,[],3), [1 1 0]); %imtool(PreviewTHMask)
    PreviewTHRaw = cat(3, imadjust(max(ch2, [], 3),[0.0015 ; 0.06]),max(chEmpty,[],3),max(chEmpty,[],3)); 
    PreviewTHRaw = imoverlay2(PreviewTHRaw, BarMask, [1 1 1]); %imtool(PreviewTHRaw)
    PreviewTHRawYellow = cat(3, imadjust(max(ch2, [], 3),[0.0015 ; 0.06]),imadjust(max(ch2, [], 3),[0.0015 ; 0.06]),max(chEmpty,[],3)); 
    PreviewTHRawYellow = imoverlay2(PreviewTHRawYellow, BarMask, [1 1 1]); %imtool(PreviewTHRaw)
     
    PreviewTuj1 = imoverlay2(imadjust(max(ch3,[],3),[0.00001 ; 0.3]), bwperim(max(Tuj1Mask,[],3)), [1 0 0]);
     PreviewTuj1 = imoverlay2(PreviewTuj1, BarMask, [1 1 1]); %imtool(PreviewTuj1)
     PreviewTuj1Mask = imoverlay2(imadjust(max(ch3,[],3),[0.00001 ; 0.3]), max(Tuj1Mask,[],3), [1 0 0]); %imtool(PreviewTuj1Mask)
     PreviewTuj1Raw = cat(3, imadjust(max(ch3, [], 3),[0.00001 ; 0.3]),max(chEmpty,[],3), max(chEmpty,[],3)); 
     PreviewTuj1Raw = imoverlay2(PreviewTuj1Raw, BarMask, [1 1 1]); %imtool(PreviewTuj1Raw)
     
     
     PreviewMAP2 = imoverlay2(imadjust(max(ch4,[],3),[0.05 ; 0.7]), bwperim(max(MAP2Mask,[],3)), [0 1 0]);
     PreviewMAP2 = imoverlay2(PreviewMAP2, BarMask, [1 1 1]); %imtool(PreviewMAP2)
     PreviewMAP2Mask = imoverlay2(imadjust(max(ch4,[],3),[0.05 ; 0.7]), max(MAP2Mask,[],3), [0 1 0]); %imtool(PreviewMAP2Mask)
     PreviewMAP2Raw = cat(3,max(chEmpty,[],3),imadjust(max(ch4, [], 3),[0.05 ; 0.7]),max(chEmpty,[],3));
     PreviewMAP2Raw  = imoverlay2(PreviewMAP2Raw , BarMask, [1 1 1]); %imtool(PreviewMAP2Raw)
     

     RGB = cat (3, imadjust(max(ch2, [], 3),[0.0001 ; 0.04]), imadjust(max(ch4, [], 3),[0.0001 ; 0.6]), imadjust(max(ch1, [], 3),[0 ; 0.1]));
     RGB = imoverlay2(RGB, BarMask, [1 1 1]); %imtool(RGB)
     
    %% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '__', Objects.AreaName{:}{:}, '_']
     imwrite(PreviewHoechst, [PreviewPath, filesep, IdentityString, 'PreviewHoechst.png'])
     imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
     imwrite(PreviewNucDead, [PreviewPath, filesep, IdentityString, 'PreviewNucDead.png'])
     imwrite(PreviewNucAliveMask, [PreviewPath, filesep, IdentityString, 'PreviewNucAliveMask.png'])
     imwrite(PreviewNucDeadMask, [PreviewPath, filesep, IdentityString, 'PreviewNucDeadMask.png'])
     
     imwrite(PreviewMAP2, [PreviewPath, filesep, IdentityString, 'PreviewMAP2.png'])
     imwrite(PreviewMAP2Mask, [PreviewPath, filesep, IdentityString, 'PreviewMAP2Mask.png'])
     imwrite(PreviewMAP2Raw, [PreviewPath, filesep, IdentityString, 'PreviewMAP2Raw.png'])
     
    imwrite(PreviewTH, [PreviewPath, filesep, IdentityString, 'PreviewTH.png'])
    imwrite(PreviewTHMask, [PreviewPath, filesep, IdentityString, 'PreviewTHMask.png'])
    imwrite(PreviewTHRaw, [PreviewPath, filesep, IdentityString, 'PreviewTHRaw.png'])
    imwrite(PreviewTHRawYellow, [PreviewPath, filesep, IdentityString, 'PreviewTHRawYellow.png'])
    
     imwrite(PreviewTuj1, [PreviewPath, filesep, IdentityString, 'PreviewTuj1.png'])
     imwrite(PreviewTuj1Mask, [PreviewPath, filesep, IdentityString, 'PreviewTuj1Mask.png'])
     imwrite(PreviewTuj1Raw, [PreviewPath, filesep, IdentityString, 'PreviewTuj1Raw.png'])
     
     imwrite(RGB, [PreviewPath, filesep, IdentityString, 'RGB.png'])
end

    
