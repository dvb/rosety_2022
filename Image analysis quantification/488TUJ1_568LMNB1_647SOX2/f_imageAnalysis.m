function [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, ch4, PreviewPath, MetaTable)

    %vol(ch1, 0, 5000) Hoechst
    %vol(ch2, 0, 4000) 647 Sox2
    %vol(ch3, 0, 3000) 568 LAMINB1
    %vol(ch4, 0, 3000) 488 TUJ1
    ObjectsThisOrganoid = table();
    %% Max projections
    %NucMax = max(ch1, [],3); imtool(NucMax,[])
    
    %% Segment Nuclei
%     NucDoG = imfilter(ch1, fspecial('gaussian', 21, 1) - fspecial('gaussian', 21, 3), 'symmetric'); % vol(NucDoG, 0, 100, 'hot')
%     NucleiMask = NucDoG > 100; %vol(NucleiMask, 0,1)

    ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 25, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 25, 5), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %figure; surf(fspecial('gaussian', 10, 5))
    ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch1DoG, 0, 2000, 'hot')
    NucleiMask = ch1DoG > 50; %vol(NucleiMask)
    %NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    NucleiMask = bwareaopen(NucleiMask, 100);%vol(NucleiMask)
    ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch3LP, 0, 3000, 'hot')
    NucMaskHigh =  (ch1LP > 2000) .* NucleiMask; %vol(NucleiMaskHigh, 0, 1)
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucleiMaskAlive)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));
    %NucleiMask = ch1 > 3000; vol(NucleiMask, 0,1)

%% TUJ1

  for p=1:size(ch4, 3)
        ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch3_FT * 1000, 0, 100, hot)

    Tuj1Mask = ch4_FT > 0.015;
    Tuj1Mask = bwareaopen(Tuj1Mask, 300);%vol(NucleiMask)
    
 % LaminDoG = imfilter(ch4, fspecial('gaussian', 11, 1) - fspecial('gaussian', 11, 3), 'symmetric'); %it(LaminDoG)
% LaminMask = LaminDoG > 60;% it(LaminMask) % vol(LaminMask)
% LaminMask = bwareaopen(LaminMask, 100);
% NucSplitv1 = NucleiMask & ~LaminMask; % it(NucSplitv1)
% % Split nuclei by shape
% %NucSplitv2 = watershed(bwdist(~NucSplitv1)) & NucSplitv1;
% NucSplitv2 = mat2gray(imcomplement(bwdist(~NucSplitv1))); % it(NucSplitv2)
% NucSplitv3 = imhmin(NucSplitv2, 0.01); % it(NucSplitv3)
% NucleiMaskSplit = watershed(NucSplitv3); % vol(NucleiMaskSplit)
% NucleiMaskSplit = logical(NucleiMaskSplit) & NucleiMask;
% 
% NucleiMaskSplit = NucleiMaskSplit & NucleiMask;
% NucleiMask = bwareafilt(NucleiMaskSplit, [50,1000]); % it(NucleiMask)
% NucStencil = bwlabeln(NucleiMask);%it(NucStencil)
% NucStencil = imdilate(NucStencil, strel('disk', 1));
% NucRGB = label2rgb(NucStencil, 'spring', 'k', 'shuffle');
% imtool(NucRGB)
%% Sox2
    Sox2DoG = imfilter(double(ch2), fspecial('gaussian', 101, 1) - fspecial('gaussian', 101, 21), 'symmetric');%vol(chSox2DoG, 0, 500, 'hot')
    Sox2Mask = Sox2DoG > 40; %vol(NucleiMask)
    Sox2Mask= Sox2Mask & NucDil;
    Sox2Mask = imclose(squeeze(Sox2Mask), strel('disk', 5));
    Sox2Mask = bwareaopen(Sox2Mask, 1000);%vol(Sox2Mask)
    Sox2Dil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));
    
    
 clear('ch2_FT') 
 
 %% LMNB1
 
    for p=1:size(ch3, 3)
    ch3MedFilt(:,:,p) = medfilt2(ch3(:,:,p)); %vol(ch2MedFilt, 0, 50000)
    end 
    ch3Blur = imfilter(ch3MedFilt, fspecial('gaussian', 10, 1), 'symmetric');%vol(ch2Blur)
    LMNB1Mask = ch3Blur > 800; %vol(LMNB1MMask)
    LMNB1MMask = LMNB1Mask & NucleiMask;
    LMNB1MMask = bwareaopen(LMNB1MMask, 100); 
	LMNB1HighMask =  ch3Blur > 1200; 
	LMNB1HighMask = LMNB1HighMask & NucleiMask;

    
    LMNB1Dil = imdilate(NucleiMask, strel('disk', 2));
    Tuj1andLMNB1 = LMNB1Dil & Tuj1Mask;
    Sox2andLMNB1 = Sox2Mask & LMNB1Mask;
    
    clear('ch3_FT') 
%% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
    Objects.Barcode = {MetaTable.Barcode};
    Objects.Batch = {MetaTable.Batch};
    Objects.Day = {MetaTable.Day};
    Objects.TotalNucMask = sum(NucleiMask(:));
	Objects.NucDeadMask = sum(NucMaskHigh(:));
    Objects.LMNB1MFI = mean(ch3(LMNB1Mask(:))) ;
	Objects.LMNB1HighMFI = mean(ch3(LMNB1HighMask(:))) ;
    Objects.LMNB1ByNuc = sum(LMNB1Mask(:)) / sum(NucleiMask(:));
	Objects.LMNB1HighByNuc = sum(LMNB1HighMask(:)) / sum(NucleiMask(:));
    Objects.LMNB1ByNucAlive = sum(LMNB1Mask(:)) / sum(NucMaskAlive(:));
    Objects.Tuj1ByNucAlive = sum(Tuj1Mask(:)) / sum(NucMaskAlive(:));
    Objects.Sox2ByNucAlive = sum(Sox2Mask(:)) / sum(NucMaskAlive(:));
    Objects.Sox2MFI = mean(ch2(Sox2Mask(:)));
    Objects.Sox2andLMNB1BySox2 = sum(Sox2andLMNB1(:)) / sum(Sox2Mask(:));
	Objects.LMNB1MFIinSox2 = mean(ch3(Sox2andLMNB1(:)));
	Objects.Tuj1andLMNB1ByTuj1 = sum(Tuj1andLMNB1(:)) / sum(Tuj1Mask(:));
	Objects.LMNB1MFIinTuj1 = mean(ch3(Tuj1andLMNB1(:)));
    
    %Objects = Iris_ObjectsInsertSubObjects(NucStencil, LaminMask, 8, ch4);
    %Objects = Objects(:, {'CountSubOjects', 'SubArea', 'Varargin1_meanSubIntensity'}); %CountSubOjects is the number of laminB structures. SubArea is the thickness of LMB1,Varargin1_meanSubIntensity is the MFI of LAMinb 

   

    %% Previews
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');

    %RGB = cat(3, imadjust(max(chVimentin, [], 3), [0 0.3], [0 1]), imadjust(max(ch3, [], 3), [0 0.99], [0 1]), imadjust(max(ch1, [], 3), [0 0.3], [0 1]));
    %imtool(RGB)
    PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
    PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]); %imtool(PreviewHoechst)
    PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0.001 ; 0.07]));
    PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)
    
    PreviewTuj1 = imoverlay2(imadjust(max(ch4,[],3),[0 0.15]), bwperim(max(Tuj1Mask,[],3)), [0 1 0]);
    PreviewTuj1 = imoverlay2(PreviewTuj1, BarMask, [1 1 1]);%imtool(PreviewTuj1)
    PreviewTuj1Mask = imoverlay2(imadjust(max(ch4,[],3),[0 0.15]), max(Tuj1Mask,[],3), [0 1 0]); %imtool(PreviewLaminMask)
    PreviewTuj1Raw = cat(3,imadjust(max(ch4, [], 3),[0.005 ; 0.035]),imadjust(max(ch4, [], 3),[0.005 ; 0.035]),max(chEmpty,[],3));
    PreviewTuj1Raw  = imoverlay2(PreviewTuj1Raw , BarMask, [1 1 1]); %imtool(PreviewTuj1Raw)
    
    PreviewLMNB1 = imoverlay2(imadjust(max(ch3,[],3),[0 0.06]), bwperim(max(LMNB1Mask,[],3)), [1 0 0]);
    PreviewLMNB1 = imoverlay2(PreviewLMNB1, BarMask, [1 1 1]);%imtool(PreviewLMNB1)
    PreviewLMNB1Mask = imoverlay2(imadjust(max(ch3,[],3),[0 0.1]), max(LMNB1Mask,[],3), [1 0 0]); %imtool(PreviewLaminMask)
    PreviewLMNB1Raw = cat(3,max(chEmpty,[],3),imadjust(max(ch3, [], 3),[0.0017 ; 0.03]),max(chEmpty,[],3));
    PreviewLMNB1Raw  = imoverlay2(PreviewLMNB1Raw , BarMask, [1 1 1]); %imtool(PreviewLMNB1Raw)

    PreviewLMNB1Dilated = imoverlay2(imadjust(max(ch4,[],3),[0 0.06]), bwperim(max(LMNB1Dil,[],3)), [0 1 0]); %imtool(PreviewTuj1Dilated)

    PreviewSox2 = imoverlay2(imadjust(max(ch2,[],3),[0 0.01]), bwperim(max(Sox2Mask,[],3)), [1 1 0]);
    PreviewSox2 = imoverlay2(PreviewSox2, BarMask, [1 1 1]);%imtool(PreviewSox2)
    PreviewSox2Mask = imoverlay2(imadjust(max(ch2,[],3),[0 0.01]), max(Sox2Mask,[],3), [1 1 0]); %imtool(PreviewLaminMask)
    PreviewSox2Raw = cat(3,imadjust(max(ch2, [], 3),[0.002 ; 0.01]),max(chEmpty,[],3),max(chEmpty,[],3));
    PreviewSox2Raw  = imoverlay2(PreviewSox2Raw , BarMask, [1 1 1]); %imtool(PreviewSox2Raw)
	
    RGB = cat(3, imadjust(max(ch3, [], 3), [0 0.08], [0 1]), imadjust(max(ch2, [], 3), [0 0.01], [0 1]), max(chEmpty,[],3)); %imtool(RGB)
    RGB_Sox2LaminB1 = cat(3, imadjust(max(ch2, [], 3),[0.002 ; 0.01]),imadjust(max(ch3, [], 3),[0.0017 ; 0.03]), max(chEmpty,[],3)); %imtool(RGB)
    RGB_LaminB1Hoechst= cat(3, max(chEmpty,[],3),imadjust(max(ch3, [], 3),[0.0017 ; 0.03]), imadjust(max(ch1, [], 3),[0.001 ; 0.07])); %imtool(RGB_LaminB1Hoechst)

%% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '__', Objects.AreaName{:}{:}, '_']
    imwrite(PreviewHoechst, [PreviewPath, filesep, IdentityString, 'PreviewHoechst.png'])
    imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
    imwrite(PreviewTuj1, [PreviewPath, filesep, IdentityString, 'PreviewTuj1.png'])
    imwrite(PreviewTuj1Mask, [PreviewPath, filesep, IdentityString, 'PreviewTuj1Mask.png'])
    imwrite(PreviewTuj1Raw, [PreviewPath, filesep, IdentityString, 'PreviewTuj1Raw.png'])
    imwrite(PreviewLMNB1, [PreviewPath, filesep, IdentityString, 'PreviewLMNB1.png'])
    imwrite(PreviewLMNB1Mask, [PreviewPath, filesep, IdentityString, 'PreviewLMNB1Mask.png'])
    imwrite(PreviewLMNB1Raw, [PreviewPath, filesep, IdentityString, 'PreviewLMNB1Raw.png'])
    imwrite(PreviewSox2, [PreviewPath, filesep, IdentityString, 'PreviewSox2.png'])
    imwrite(PreviewSox2Mask, [PreviewPath, filesep, IdentityString, 'PreviewSox2Mask.png'])
    imwrite(PreviewSox2Raw, [PreviewPath, filesep, IdentityString, 'PreviewSox2Raw.png'])
	imwrite(RGB_Sox2LaminB1, [PreviewPath, filesep, IdentityString, 'RGB_Sox2LaminB1.png'])
    imwrite(RGB_LaminB1Hoechst, [PreviewPath, filesep, IdentityString, 'RGB_LaminB1Hoechst.png'])
end

