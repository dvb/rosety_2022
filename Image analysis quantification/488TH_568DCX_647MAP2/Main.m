%% Debug test
%% User inputs
%/mnt/irisgpfs/projects/lcsb_hcs/Advanced/IsabelRosety/GBAOrganoids
delete(gcp('nocreate'))
myCluster = parcluster('local');
AvailableWorkers = myCluster.NumWorkers;
if AvailableWorkers >= 7
    pool = parpool(7)
else
    pool = parpool(1)
end
addpath(genpath('/work/projects/lcsb_hcs/Library/hcsforge'))
addpath(genpath('/work/projects/lcsb_hcs/Library/hcsIris'))

% if ~exist('InPath') % otherwise use the one defined via the database
%     InPath = '/work/projects/lcsb_hcs/Data/IsabelRosety/IR_20210801_488TH_568Sox2_647MAP2_RS_e35to37e31to33d60_20210801_160423_in';
% end

InPath = '/work/projects/lcsb_hcs/Data/IsabelRosety/IR_20211129_488TH_568DCX_647MAP2_RS_e45e46e47d30_20211129_174319_in';

%%
MesPath = ls([InPath, '/*.mes']); MesPath = MesPath(1:end-1); % remove line break
MetaData = f_CV8000_getChannelInfo(InPath, MesPath);

% if ~exist('OutPath') % if it doesnt exist, do this, otherwise use the one defined via the database
%     %OutPath =
%     %'/mnt/irisgpfs/projects/lcsb_hcs/Data/IsabelRosety/IR_20210217_488MAP2TUJ1_647TH_RS_e25e26e27e28d30_20210223_182707_in';
% 
%     %OutPath = '/mnt/D/Isabel20210616';%I don have writing permission on Data, so when debugging, change Outpath
% end

OutPath = '/mnt/lscratch/users/irosety/Results/IR_20211129_488TH_568DCX_647MAP2_RS_e45e46e47d30_20211129_174319';
%Barcode='IR_20210820_488TH_568Sox2_647MAP2_RS_e35to37e31to33d60_20210820_101636';
%Day='D60';
Barcode = regexp(InPath, '.*(IR_.*)_in', 'tokens');
Barcode = Barcode{:}{:};
Day = regexp(InPath, '.*(d\d{2}).*', 'tokens');
Day = Day{:}{:};
%% Load Metadata
MetaData = f_CV8000_getChannelInfo(InPath, MesPath);
%% Prepare folders
mkdir(OutPath)
ThumbnailPath = [OutPath, filesep, 'Thumbnails'];
mkdir(ThumbnailPath)
PreviewPath = [OutPath, filesep, 'Previews'];


mkdir(PreviewPath)
% %% Creating a copy of the Main script that is running (the copyfile fucntion only works if you run the script not if you F9)
FileNameShort = mfilename;
% newbackup = sprintf('%s_log.m',[OutPath, '\', FileNameShort]);
% FileNameAndLocation = [mfilename('fullpath')];
% currentfile = strcat(FileNameAndLocation, '.m');
% copyfile(currentfile,newbackup); 
% % Creating a file with the version that is running of Matlab
% Version = version();
% save([OutPath filesep 'MatlabVersion.mat'], 'Version')
% % Triggering fucntion of Dependencies -> This creates a folder having a copy of all the custom functions present in all the layers of the script
% f_LogDependencies_Cluster(FileNameShort, OutPath); 
%f_LogDependenciesLinux(FileNameShort, OutPath); 


%% Analyze selected organoids
InfoTable = MetaData.InfoTable{:};
PlaneCount = max(cell2mat(cellfun(@(x) str2double(x), InfoTable.Plane, 'UniformOutput', false)));
ChannelCount = max(cell2mat(cellfun(@(x) str2double(x), InfoTable.Channel, 'UniformOutput', false)));
% Read SlideLayout
try
    OrganoidsToAnalyze = readtable([InPath, filesep, 'SlideLayout.csv'], 'ReadVariableNames', false);
    
    %OrganoidsToAnalyze = readtable([InPath,filesep, 'SlideLayout.csv'],'Delimiter', ',', 'ReadVariableNames', false);
catch
    OrganoidsToAnalyze = readtable(['.', filesep, 'SlideLayout.csv'], 'ReadVariableNames', false);
end
%OrganoidsToAnalyze.Properties.VariableNames = {'Idx', 'Well', 'AreaName'};
OrganoidsToAnalyze.Properties.VariableNames = {'Idx', 'Well', 'AreaName','Batch'};
OrganoidsToAnalyze.OrganoidID = rowfun(@(a,b) sprintf('%s_%s', a, b), OrganoidsToAnalyze, 'InputVariables', {'Well', 'Idx'}, 'ExtractCellContents', true, 'OutputFormat', 'cell');

try
    Coordinates = readtable([InPath, filesep, 'Encoding.csv'], 'ReadVariableNames', true);
catch
    Coordinates = readtable(['.', filesep, 'Encoding.csv'], 'ReadVariableNames', true);
end

OrganoidsToAnalyze.Barcode(:,1) = {Barcode};
OrganoidsToAnalyze.Day(:,1) = {Day};
    
%[FieldAssignments, OrganoidStencil] = Iris_GetOrganoidFields(InPath, MetaData, overviewSlide, Coordinates, false);
[FieldAssignmentsAll, OrganoidStencilsAll, WellsAll] = Iris_GetOrganoidFields(InPath, MetaData, Coordinates, false);
%[FieldAssignments, OrganoidStencils, Wells] = Iris_GetOrganoidFields(InPath, MetaData, Coordinates, true);
% imtool(flip(OrganoidStencil,1),[])
% imtool(flip(OrganoidStencils{1},1),[])
% imtool(flip(OrganoidStencils{2},1),[])

ObjectsAll = {};
for organoid = 1:size(OrganoidsToAnalyze,1)
%for organoid = 8
    try
        tic
        disp(['organoidID is ', num2str(organoid)])
        disp(['Well ', OrganoidsToAnalyze{organoid, 'Well'}{:}, '__Organoid #' num2str(organoid)])
        OrganoidThis = OrganoidsToAnalyze(organoid, :);
        SlideThis = OrganoidThis.Well{:};
        Sample = OrganoidThis.AreaName{:};
        ID = OrganoidThis.Idx;
        OrganoiIdxInThisSlide = regexp(ID{:}, 'OR(\d+)', 'tokens'); %%% Fixed bug that skips slides 2,3,4
        OrganoiIdxInThisSlide = str2num(OrganoiIdxInThisSlide{:}{:}) %%% Fixed bug that skips slides 2,3,4
        skipAll = false;
        if ~skipAll
            %% coordinate slide specific data
            ThisWellString = OrganoidsToAnalyze{organoid, 'Well'}{:};
            SlideIdx = find(strcmp(WellsAll, ThisWellString));
            FieldAssignments = FieldAssignmentsAll{SlideIdx};
            OrganoidStencils = OrganoidStencilsAll{SlideIdx};

            %% Load organoid rescan image mosaic (optimized for speed)
            ImSize = size(imread(InfoTable{1,'file'}{:}));
            %FieldsThisOrganoid = FieldAssignments(FieldAssignments.OrganoidID == organoid, :);
            FieldsThisOrganoid = FieldAssignments(FieldAssignments.OrganoidID == OrganoiIdxInThisSlide, :); %%% Fixed bug that skips slides 2,3,4
            XVecThisOrganoid = sort(unique(FieldsThisOrganoid.X));
            YVecThisOrganoid = sort(unique(FieldsThisOrganoid.Y));
            ch1Meta=table();
            ch2Meta=table();
            ch3Meta=table();
            ch4Meta=table();
            ch5Meta=table();
            progressCh1 = 0;
            progressCh2 = 0;
            progressCh3 = 0;
            progressCh4 = 0;
            %progressCh5 = 0;

            for ch = 1:ChannelCount
                for field = 1:height(FieldsThisOrganoid)
                    for plane = 1:PlaneCount
                        FieldThis = FieldsThisOrganoid.Field(field);
                        ThisTilePath = sortrows(InfoTable(strcmp(InfoTable.Well, ThisWellString) & strcmp(InfoTable.Field, sprintf('%03d', FieldThis)) & strcmp(InfoTable.Plane, sprintf('%02d', plane)), :), 'Channel');
                        StartRowThisTile =    ((length(YVecThisOrganoid) - (find(YVecThisOrganoid == FieldsThisOrganoid.Y(field)))) * ImSize(1)) + 1;
                        StartColumnThisTile = (((find(XVecThisOrganoid == FieldsThisOrganoid.X(field))) -1) * ImSize(2)) + 1;

                        switch ch
                            case 1
                                progressCh1 = progressCh1 + 1;
                                ch1Meta(progressCh1, 'StartRowThisTile') = {StartRowThisTile};
                                ch1Meta(progressCh1, 'StartColumnThisTile') = {StartColumnThisTile};
                                ch1Meta(progressCh1, 'Plane') = {plane};
                                ch1Meta(progressCh1, 'Field') = {field};
                                ch1Meta(progressCh1, 'ThisTilePath') = ThisTilePath.file(1);
                            case 2
                                progressCh2 = progressCh2 + 1;
                                ch2Meta(progressCh2, 'StartRowThisTile') = {StartRowThisTile};
                                ch2Meta(progressCh2, 'StartColumnThisTile') = {StartColumnThisTile};
                                ch2Meta(progressCh2, 'Plane') = {plane};
                                ch2Meta(progressCh2, 'Field') = {field};
                                ch2Meta(progressCh2, 'ThisTilePath') = ThisTilePath.file(2);
                            case 3
                                progressCh3 = progressCh3 + 1;
                                ch3Meta(progressCh3, 'StartRowThisTile') = {StartRowThisTile};
                                ch3Meta(progressCh3, 'StartColumnThisTile') = {StartColumnThisTile};
                                ch3Meta(progressCh3, 'Plane') = {plane};
                                ch3Meta(progressCh3, 'Field') = {field};
                                ch3Meta(progressCh3, 'ThisTilePath') = ThisTilePath.file(3);
                            case 4
                                progressCh4 = progressCh4 + 1;
                                ch4Meta(progressCh4, 'StartRowThisTile') = {StartRowThisTile};
                                ch4Meta(progressCh4, 'StartColumnThisTile') = {StartColumnThisTile};
                                ch4Meta(progressCh4, 'Plane') = {plane};
                                ch4Meta(progressCh4, 'Field') = {field};
                                ch4Meta(progressCh4, 'ThisTilePath') = ThisTilePath.file(4);
    %                         case 5
    %                             progressCh5 = progressCh5 + 1;
    %                             ch5Meta(progressCh5, 'StartRowThisTile') = {StartRowThisTile};
    %                             ch5Meta(progressCh5, 'StartColumnThisTile') = {StartColumnThisTile};
    %                             ch5Meta(progressCh5, 'Plane') = {plane};
    %                             ch5Meta(progressCh5, 'Field') = {field};
    %                             ch5Meta(progressCh5, 'ThisTilePath') = ThisTilePath.file(5);
                        end
                    end
                end
            end
            ch1Ims = {};
            ch2Ims = {};
            ch3Ims = {};
            ch4Ims = {};
    %         ch5Ims = {};

            parfor chi = 1:(height(ch1Meta))
                ch1Ims{chi} = imread(ch1Meta.ThisTilePath{chi});
                ch2Ims{chi} = imread(ch2Meta.ThisTilePath{chi});
                ch3Ims{chi} = imread(ch3Meta.ThisTilePath{chi});
                ch4Ims{chi} = imread(ch4Meta.ThisTilePath{chi});
    %             ch5Ims{chi} = imread(ch5Meta.ThisTilePath{chi});
            end

            Planes = numel(unique(ch1Meta.Plane));
            
            ch1 = zeros(length(YVecThisOrganoid) * ImSize(1), length(XVecThisOrganoid) * ImSize(2), Planes, 'uint16');
            for i = 1:height(ch1Meta)
                ch1(ch1Meta{i, 'StartRowThisTile'}:ch1Meta{i, 'StartRowThisTile'} + ImSize(1)-1, ch1Meta{i, 'StartColumnThisTile'}:ch1Meta{i, 'StartColumnThisTile'} + ImSize(2)-1, ch1Meta{i, 'Plane'}) = ch1Ims{i};
            end
            %vol(ch1, 0, 5000)
            
            ch2 = zeros(length(YVecThisOrganoid) * ImSize(1), length(XVecThisOrganoid) * ImSize(2), Planes, 'uint16');
            for i = 1:height(ch1Meta)
                ch2(ch2Meta{i, 'StartRowThisTile'}:ch2Meta{i, 'StartRowThisTile'} + ImSize(1)-1, ch2Meta{i, 'StartColumnThisTile'}:ch2Meta{i, 'StartColumnThisTile'} + ImSize(2)-1, ch2Meta{i, 'Plane'}) = ch2Ims{i};
            end
            %vol(ch2, 0, 5000)
            
            ch3 = zeros(length(YVecThisOrganoid) * ImSize(1), length(XVecThisOrganoid) * ImSize(2), Planes, 'uint16');
            for i = 1:height(ch1Meta)
                ch3(ch3Meta{i, 'StartRowThisTile'}:ch3Meta{i, 'StartRowThisTile'} + ImSize(1)-1, ch3Meta{i, 'StartColumnThisTile'}:ch3Meta{i, 'StartColumnThisTile'} + ImSize(2)-1, ch3Meta{i, 'Plane'}) = ch3Ims{i};
            end
            %vol(ch3, 0, 5000)
            ch4 = zeros(length(YVecThisOrganoid) * ImSize(1), length(XVecThisOrganoid) * ImSize(2), Planes, 'uint16');
            for i = 1:height(ch1Meta)
                ch4(ch4Meta{i, 'StartRowThisTile'}:ch4Meta{i, 'StartRowThisTile'} + ImSize(1)-1, ch4Meta{i, 'StartColumnThisTile'}:ch4Meta{i, 'StartColumnThisTile'} + ImSize(2)-1, ch4Meta{i, 'Plane'}) = ch4Ims{i};
            end
            %vol(ch4, 0, 5000)
    %         ch5 = zeros(length(YVecThisOrganoid) * ImSize(1), length(XVecThisOrganoid) * ImSize(2), Planes, 'uint16');
    %         for i = 1:height(ch1Meta)
    %             ch5(ch5Meta{i, 'StartRowThisTile'}:ch5Meta{i, 'StartRowThisTile'} + ImSize(1)-1, ch5Meta{i, 'StartColumnThisTile'}:ch5Meta{i, 'StartColumnThisTile'} + ImSize(2)-1, ch5Meta{i, 'Plane'}) = ch5Ims{i};
    %         end
            %vol(ch5, 0, 5000)
            OrganoidLoadTime = toc;
            disp(['Needed ', num2str(OrganoidLoadTime), ' seconds to load organoid images'])


            clear 'ch1Ims' 'ch2Ims' 'ch3Ims' 'ch4Ims' 'ch5Ims'
    %         chFindBestPlanes = uint32(ch1)+ uint32(ch2) + uint32(ch3) + uint32(ch4); % no BF as too bright
            chFindBestPlanes = uint32(ch1)+ uint32(ch2) + uint32(ch3); % no BF as too bright
            chFindBestPlanesSummary = squeeze(sum(chFindBestPlanes,[1,2])); % plane sum intensity vector
            %figure; plot(chFindBestPlanesSummary)
            GoodPlanes = chFindBestPlanesSummary > (max(chFindBestPlanesSummary)/2.5); % Above 40% of maximum
            GoodPlanes = bwareafilt(GoodPlanes, 1);
            clear 'chFindBestPlanes'
            ch1 = ch1(:,:,GoodPlanes); % vol(ch1, 0, 10000)
            ch2 = ch2(:,:,GoodPlanes);
            ch3 = ch3(:,:,GoodPlanes);
            ch4 = ch4(:,:,GoodPlanes);
    %         ch5 = ch5(:,:,GoodPlanes);

            exploration = 0 % put 1 when debugging the script and 0 when we want to run it
            if exploration
                ch1 = ch1(2000:3000,3000:4000,GoodPlanes); %vol(ch1,0,5000)
                ch2 = ch2(2000:3000,3000:4000,GoodPlanes);
                ch3 = ch3(2000:3000,3000:4000,GoodPlanes);
                ch4 = ch4(2000:3000,3000:4000,GoodPlanes);
            end


            ObjectsThisOrganoid = f_imageAnalysis(num2str(organoid), ch1, ch2, ch3, ch4, PreviewPath, OrganoidsToAnalyze(organoid, :));
            %ObjectsThisOrganoid = f_imageAnalysis(num2str(organoid), ch1, ch2, ch3, PreviewPath, OrganoidsToAnalyze(organoid, :));
            ObjectsAll{organoid} = ObjectsThisOrganoid;
        end
    catch E
        disp(E)
        continue
    end
end

data = vertcat(ObjectsAll{:});
save([OutPath, filesep, 'data.mat'], 'data')
%writetable(data, [OutPath, filesep, 'data.csv'])
writetable(data, [OutPath, filesep,Barcode, '.csv'])


%% Copy of the script

% function [FileList] = f_LogDependenciesLinux(Name, SavePath)
% %Collect all upstream scripts and save them in a folder named Dependencies
% %   Name: Function or script name as string
% %   SavePath: 
% %   Example: f_LogDependencies('Workflow_RosellaLC3_BPF_Summary_20170313.m', 'S:\HCS_Platform\Data\JavierJarazo\Rosella\LC3\Analysis_20170323_143443')
%     
%     SavePath = [SavePath filesep 'Dependencies'];
%     mkdir(SavePath);
%     
%     %% Scan recursively %%
%     FileList = matlab.codetools.requiredFilesAndProducts(Name)';
%     %%%%%%%%%%%%%%%%%%%%%%
%     FileCount = size(FileList,1);
% 
%     for i = 1:FileCount
%         FileThis = regexp(FileList{i}, '/.*/(.*)', 'tokens'); FileThis = FileThis{:}{:};
%         SavePathThisFile = [SavePath, filesep, FileThis];
%         copyfile(FileList{i}, SavePathThisFile);
%     end
%     
% end