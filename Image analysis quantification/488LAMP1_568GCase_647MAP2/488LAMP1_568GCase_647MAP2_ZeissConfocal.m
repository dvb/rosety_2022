%% Prepare Matlab
clear
clc
addpath(genpath('S:\Libraries\hcsforge\LCSBMatlabLibrary'))

%% Document script
%SavePathMainDirectory = 'S:\HCS_Platform\Data\SilviaBolognin\HumanSamples\LAMNB1\';
SavePathMainDirectory = 'S:\HCS_Platform\Data\IsabelRosety\Confocal\GBA_LAMP1_MAP2\20211209_488LAMP1_568GCase_647MAP2';

AnalysisTimeStamp = datestr(now, 'yyyymmdd_HHMMSS');
SavePath = [SavePathMainDirectory, 'Analysis_', AnalysisTimeStamp];
mkdir(SavePath)
FileNameShort = mfilename;
newbackup = sprintf('%s_log.m',[SavePath, '\', FileNameShort]);
FileNameAndLocation = [mfilename('fullpath')];
currentfile = strcat(FileNameAndLocation, '.m');
copyfile(currentfile,newbackup);
PreviewPath = [SavePath, filesep, 'Previews']
mkdir(PreviewPath);

%% Document Matlab state
Version = ver;
MegatronPath = pwd;

%% Analysis
Objects = {};

files = dirrec('S:\HCS_Platform\Data\IsabelRosety\Confocal\GBA_LAMP1_MAP2\20211209_488LAMP1_568GCase_647MAP2', '.czi')';
for s = 1:size(files,1)
    
    sample = files{s};
    data = bfopen(sample);

    %CellLine = regexp(sample, 'SB_(.*).czi', 'tokens');
    CellLine = regexp(sample, 'IR_(.*).czi', 'tokens');
    CellLine = CellLine{:}{:};

    %% Extract metadata from omeXML
    omeMeta = data{1, 4};
    SizeX = omeMeta.getPixelsSizeX(0).getValue(); % image width, pixels
    SizeY = omeMeta.getPixelsSizeY(0).getValue(); % image height, pixels
%     SizeZ = omeMeta.getPixelsSizeZ(0).getValue(); % number of Z slices
    SizeT = omeMeta.getPixelsSizeT(0).getValue(); % number of timepoints
    SizeC = omeMeta.getPixelsSizeC(0).getValue(); % number of channels
    voxelSizeX = double(omeMeta.getPixelsPhysicalSizeX(0).value(ome.units.UNITS.MICROM)); % in �m
    voxelSizeY = double(omeMeta.getPixelsPhysicalSizeY(0).value(ome.units.UNITS.MICROM)); % in �m
%     voxelSizeZ = double(omeMeta.getPixelsPhysicalSizeZ(0).value(ome.units.UNITS.MICROM)); % in �m

    Fluors = {};
    for chID = 1:SizeC
        Fluors{chID} = char(omeMeta.getChannelFluor(0,chID-1))
    end
    %FluorsSilvia = {'TH', 'LAMINB1', 'Hoechst', 'GFAP'};

    % Load volume data
    ch1 = cat(3, data{1,1}{(1:SizeC:size(data{1,1},1)),1}); % Hoechst   % vol(ch1, 0, 15000)
    ch2 = cat(3, data{1,1}{(2:SizeC:size(data{1,1},1)),1}); % MAP2   % vol(ch2, 0, 15000)
    ch3 = cat(3, data{1,1}{(3:SizeC:size(data{1,1},1)),1}); % GBA  % vol(ch3, 0, 50000)
    ch4 = cat(3, data{1,1}{(4:SizeC:size(data{1,1},1)),1}); % LAMP1   % vol(ch4, 0, 50000)
    
%% Initialize variables
    NucleiMask = [];
    LAMP1Mask = [];
    GBAMask = [];
    MAP2Mask = [];
    
%% Segment nuclei (ch1)   
    %vol(ch3, 0, 1000)
    ch1BlurSmall = imfilter(ch1, fspecial('gaussian', 10, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(ch1, fspecial('gaussian', 300, 100), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    ch1DoG = ch1BlurSmall - ch1BlurBig; % vol(ch1DoG, 0, 50)
    
    NucleiMask = ch1DoG>20; %vol(NucleiMask)
    PykNuclei= NucleiMask >500;
    NucleiMask = NucleiMask &~ PykNuclei;
    NucleiMask = bwareaopen(NucleiMask, 20000);
   
%% LAMP1

  for p=1:size(ch4, 3)
        %ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
        ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch3_FT * 1000, 0, 100, hot)

    LAMP1Mask = ch4_FT > 0.15;
    LAMP1Mask = bwareaopen(LAMP1Mask, 50);%vol(NucleiMask)
    
       
 clear('ch4_FT') 
%% MAP2
  for p=1:size(ch2, 3)
        ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [15,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch3_FT * 1000, 0, 100, hot)

    MAP2Mask = ch2_FT > 0.07;
    MAP2Mask = bwareaopen(MAP2Mask, 300);%vol(NucleiMask)

 clear('ch2_FT') 
 
 %% GBA
   for p=1:size(ch3, 3)
        %ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
        ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch3_FT * 1000, 0, 100, hot)

    GBAMask = ch3_FT > 0.08;
    GBAMask = bwareaopen(GBAMask, 50);%vol(NucleiMask)
   
    clear('ch3_FT') 
    %% Colocalization masks
    MAP2andLAMP1 = MAP2Mask & LAMP1Mask;
    MAP2andGBA = MAP2Mask & GBAMask;
    GBAandLAMP1 = GBAMask & LAMP1Mask;
    GBAandLAMP1inMAP2 = GBAandLAMP1 & MAP2Mask;

    %% Previews 
    %Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(10, 0.07, imSize, imSize(1)-100, 800, 10);
    %it(BarMask)
    
    PreviewLAMP1 = imoverlay2(imadjust(max(ch4,[],3),[0 0.9]), bwperim(max(LAMP1Mask,[],3)), [0 1 0]);
    PreviewLAMP1 = imoverlay2(PreviewLAMP1, BarMask, [1 1 1]);%imtool(PreviewLAMP1)
    PreviewLAMP1Mask = imoverlay2(imadjust(max(ch4,[],3),[0 0.9]), max(LAMP1Mask,[],3), [0 1 0]); %imtool(PreviewLaminMask)

    PreviewGBA = imoverlay2(imadjust(max(ch3,[],3),[0 0.9]), bwperim(max(GBAMask,[],3)), [1 0 0]);
    PreviewGBA = imoverlay2(PreviewGBA, BarMask, [1 1 1]);%imtool(PreviewGBA)
    PreviewGBAMask = imoverlay2(imadjust(max(ch3,[],3),[0 0.9]), max(GBAMask,[],3), [1 0 0]); %imtool(PreviewLaminMask)

    PreviewMAP2 = imoverlay2(imadjust(max(ch2,[],3),[0 0.3]), bwperim(max(MAP2Mask,[],3)), [1 1 0]);
    PreviewMAP2 = imoverlay2(PreviewMAP2, BarMask, [1 1 1]);%imtool(PreviewMAP2)
    PreviewMAP2Mask = imoverlay2(imadjust(max(ch2,[],3),[0 0.05]), max(MAP2Mask,[],3), [1 1 0]); %imtool(PreviewLaminMask)

    %it(PreviewLAMINB1)
    %it(PreviewLAMINB1Mask)
    
    
    PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0 0.98]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
    PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]);
    %it(PreviewHoechst)


     PreviewRGB = cat(3, imadjust(max(ch2,[],3)), imadjust(max(ch3,[],3)), imadjust(max(ch4,[],3)));
     PreviewRGB = imoverlay2(PreviewRGB, BarMask, [1 1 1]);
    %it(PreviewRGB)
    
    imwrite(PreviewMAP2, [PreviewPath, filesep, CellLine, '_', 'MAP2', '_', num2str(s), '.png'])
    imwrite(PreviewGBA, [PreviewPath, filesep, CellLine, '_', 'GBA', '_', num2str(s), '.png'])
    imwrite(PreviewLAMP1, [PreviewPath, filesep, CellLine, '_', 'LAMP1', '_', num2str(s), '.png'])
    imwrite(PreviewHoechst, [PreviewPath, filesep, CellLine, '_', 'Hoechst', '_', num2str(s), '.png'])
   %imwrite(PreviewRGB, [PreviewPath, filesep, CellLine,'_','LAMINB1_TH_GFAP', '_', num2str(s), '.png'])



    %% Feature extraction    
    Objects = table();
    Objects.CellLine = {CellLine};
    Objects.NucMaskSum = sum(NucleiMask(:));
    Objects.GBAMFI = mean(ch3(GBAMask(:))) ;
	Objects.LAMP1MFI = mean(ch4(LAMP1Mask(:))) ;
    Objects.LAMP1ByNuc = sum(LAMP1Mask(:)) / sum(NucleiMask(:));
    Objects.GBAByNuc = sum(GBAMask(:)) / sum(NucleiMask(:));
    Objects.MAP2andLAMP1ByMAP2 = sum(MAP2andLAMP1(:)) / sum(MAP2Mask(:));
    Objects.MAP2andGBAByMAP2 = sum(MAP2andGBA(:)) / sum(MAP2Mask(:));
    Objects.GBAandLAMP1ByGBA = sum(GBAandLAMP1(:)) / sum(GBAMask(:));
    Objects.GBAandLAMP1inMAP2ByMAP2 = sum(GBAandLAMP1inMAP2(:)) / sum(MAP2Mask(:));
    
    if s == 1
        ObjectsAll = Objects;
    else
        ObjectsAll = [ObjectsAll; Objects];
    end
   
end

save([SavePath, filesep, 'Objects.mat'], 'ObjectsAll', 'Version')
writetable(ObjectsAll, [SavePath, filesep, 'Objects.xls'])
    
    
    
    
    
    
    
    
    
    
    
    