function  [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, ch4, PreviewPath, MetaTable)
    %vol(ch1, 0, 5000) Hoechst
    %vol(ch2, 0, 4000) 647 MAP2 (270 Mouse)
    %vol(ch3, 0, 5000) 568 TH (111 Chicken)
    %vol(ch4, 0, 5000) 488 Caspase3
    ObjectsThisOrganoid = table();
    %% Max projections
    %NucMax = max(ch1, [],3); imtool(NucMax,[])
    
    %% Segment Nuclei
%     NucDoG = imfilter(ch1, fspecial('gaussian', 21, 1) - fspecial('gaussian', 21, 3), 'symmetric'); % vol(NucDoG, 0, 100, 'hot')
%     NucMask = NucDoG > 100; %vol(NucMask, 0,1)

    ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 25, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 25, 5), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %figure; surf(fspecial('gaussian', 10, 5))
    ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch1DoG, 0, 2000, 'hot')
    NucleiMask = ch1DoG > 50; %vol(NucleiMask)
    %NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    NucleiMask = bwareaopen(NucleiMask, 50);%vol(NucleiMask)
    ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch3LP, 0, 3000, 'hot')
    NucMaskHigh =  (ch1LP > 2000) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucMaskAlive)
    %NucMask = ch1 > 3000; vol(NucMask, 0,1)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));

    %% MAP2
   for p=1:size(ch2, 3)
        ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [15,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [7,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch2_FT * 1000, 0, 100, hot)

    MAP2Mask = ch2_FT > 0.01;
    MAP2Mask = bwareaopen(MAP2Mask, 1000);
    
 clear('ch2_FT')   

    %% Caspase3
    
    ch4BlurSmall = imfilter(double(ch4), fspecial('gaussian', 25, 1), 'symmetric');%vol(ch3BlurSmall)
    ch4BlurBig = imfilter(double(ch4), fspecial('gaussian', 25, 5), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %figure; surf(fspecial('gaussian', 10, 5))
    ch4DoG = ch4BlurSmall - ch4BlurBig; %vol(ch1DoG, 0, 2000, 'hot')
    Caspase3Mask = ch4DoG > 50; %vol(NucleiMask)
    Caspase3Mask = NucleiMask & Caspase3Mask;
    %NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    Caspase3Mask = bwareaopen(Caspase3Mask, 300);%vol(NucleiMask)
    Cas3Dil = imdilate(imdilate(Caspase3Mask, strel('disk', 3)),strel('sphere',1));
    
    MAP2andCaspase3 = MAP2Mask & Cas3Dil;
    
 clear('ch4DoG') 
     %% TH

    for p=1:size(ch3, 3)
        ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [15,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [7,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch2_FT * 1000, 0, 100, hot)

    THMask = ch3_FT > 0.005;
    THMask = bwareaopen(THMask, 100);
    THandCaspase3 = THMask & Cas3Dil;
    THandMAP2= THMask & MAP2Mask;
 clear('ch2_FT') 
    
    %% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
    Objects.Barcode = {MetaTable.Barcode};
    Objects.Batch = {MetaTable.Batch};
    Objects.Day = {MetaTable.Day};
    Objects.MAP2Mask = sum(MAP2Mask(:));
	%Objects.MAP2MFI = mean(ch3(MAP2Mask));
    Objects.Caspase3Mask = sum(Caspase3Mask(:));
    Objects.THMask = sum(Caspase3Mask(:));
    Objects.TotalNucMask = sum(NucleiMask(:));
	Objects.NucDeadMask = sum(NucMaskHigh(:));
	Objects.THandMAP2Mask = sum(THandMAP2(:));    
 	Objects.THandMAP2ByMAP2 = sum(THandMAP2(:)) / sum(MAP2Mask(:));
	Objects.Caspase3ByNuc = sum(Caspase3Mask(:)) / sum(NucleiMask(:));
	Objects.THByNucAlive = sum(THMask(:)) / sum(NucMaskAlive(:));
    Objects.MAP2ByNucAlive = sum(MAP2Mask(:)) / sum(NucMaskAlive(:));
	Objects.THandCas3ByTH = sum(THandCaspase3(:)) / sum(THMask(:));
	Objects.THandCas3ByNuc = sum(THandCaspase3(:)) / sum(NucleiMask(:));    
	Objects.MAP2andCas3ByMAP2 = sum(MAP2andCaspase3(:)) / sum(MAP2Mask(:));
	Objects.MAP2andCas3ByNuc = sum(MAP2andCaspase3(:)) / sum(NucleiMask(:));
    
    
    %% Previews 
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');

    PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
    PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]); %imtool(PreviewHoechst)
    PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0 ; 0.07]));
    PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)
    PreviewNucDead = imoverlay2(imadjust(max(ch1,[],3),[0 0.1]), bwperim(max(NucMaskHigh,[],3)), [0 0 1]);
    PreviewNucDead = imoverlay2(PreviewNucDead, BarMask, [1 1 1]); %imtool(PreviewNucDead)
    PreviewNucDeadMask = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), max(NucMaskHigh,[],3), [0 0 1]); %imtool(PreviewNucDeadMask)
    PreviewNucAlive = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), bwperim(max(NucMaskAlive,[],3)), [0 0 1]);
    PreviewNucAlive = imoverlay2(PreviewNucAlive, BarMask, [1 1 1]); %imtool(PreviewNucAlive)
    PreviewNucAliveMask = imoverlay2(imadjust(max(ch1,[],3),[0 0.1]), max(NucMaskAlive,[],3), [0 0 1]); %imtool(PreviewNucAliveMask)
    
    PreviewTH = imoverlay2(imadjust(max(ch3,[],3),[0 0.02]), bwperim(max(THMask,[],3)), [1 1 0]);
    PreviewTH = imoverlay2(PreviewTH, BarMask, [1 1 1]); %imtool(PreviewTH)
    PreviewTHMask = imoverlay2(imadjust(max(ch3,[],3),[0 0.02]), max(THMask,[],3), [1 1 0]); %imtool(PreviewTHMask)
    PreviewTHRaw = cat(3, imadjust(max(ch3, [], 3),[0.0001 ; 0.02]), imadjust(max(ch3, [], 3),[0.0001 ; 0.1]),max(chEmpty,[],3)); 
    PreviewTHRaw = imoverlay2(PreviewTHRaw, BarMask, [1 1 1]); %imtool(PreviewTHRaw)
    
    PreviewMAP2 = imoverlay2(imadjust(max(ch2,[],3),[0 0.04]), bwperim(max(MAP2Mask,[],3)), [1 0 0]);
    PreviewMAP2 = imoverlay2(PreviewMAP2, BarMask, [1 1 1]); %imtool(PreviewMAP2)
    PreviewMAP2Mask = imoverlay2(imadjust(max(ch2,[],3),[0 0.04]), max(MAP2Mask,[],3), [1 0 0]); %imtool(PreviewMAP2Mask)
    PreviewMAP2Raw = cat(3, imadjust(max(ch2, [], 3),[0.0001 ; 0.04]),max(chEmpty,[],3), max(chEmpty,[],3)); 
    PreviewMAP2Raw = imoverlay2(PreviewMAP2Raw, BarMask, [1 1 1]); %imtool(PreviewMAP2Raw)
    
    PreviewMAP2andCas3 = imoverlay2(imadjust(max(ch2,[],3),[0 0.04]), max(MAP2andCaspase3,[],3), [1 0 0]); %imtool(PreviewMAP2andCas3)
    
    PreviewCaspase3 = imoverlay2(imadjust(max(ch4,[],3),[0 0.05]), bwperim(max(Caspase3Mask,[],3)), [0 1 0]);
    PreviewCaspase3 = imoverlay2(PreviewCaspase3, BarMask, [1 1 1]); %imtool(PreviewCaspase3)
    PreviewCaspase3Mask = imoverlay2(imadjust(max(ch4,[],3),[0 0.3]), max(Caspase3Mask,[],3), [0 1 0]); %imtool(PreviewCaspase3Mask)
    PreviewCaspase3Raw = cat(3,max(chEmpty,[],3),imadjust(max(ch4, [], 3),[0.0001 ; 0.1]),max(chEmpty,[],3));
    PreviewCaspase3Raw  = imoverlay2(PreviewCaspase3Raw , BarMask, [1 1 1]); %imtool(PreviewCaspase3Raw)
    

    RGB = cat (3, imadjust(max(ch2, [], 3),[0.0001 ; 0.04]), imadjust(max(ch4, [], 3),[0.0001 ; 0.4]), imadjust(max(ch1, [], 3),[0 ; 0.1]));
    RGB = imoverlay2(RGB, BarMask, [1 1 1]); %imtool(RGB)
    
    %% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '__', Objects.AreaName{:}{:}, '_']
    imwrite(PreviewHoechst, [PreviewPath, filesep, IdentityString, 'PreviewHoechst.png'])
    imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
    imwrite(PreviewNucDead, [PreviewPath, filesep, IdentityString, 'PreviewNucDead.png'])
    imwrite(PreviewNucAliveMask, [PreviewPath, filesep, IdentityString, 'PreviewNucAliveMask.png'])
    imwrite(PreviewNucDeadMask, [PreviewPath, filesep, IdentityString, 'PreviewNucDeadMask.png'])
    
    imwrite(PreviewCaspase3, [PreviewPath, filesep, IdentityString, 'PreviewCaspase3.png'])
    imwrite(PreviewCaspase3Mask, [PreviewPath, filesep, IdentityString, 'PreviewCaspase3Mask.png'])
    imwrite(PreviewCaspase3Raw, [PreviewPath, filesep, IdentityString, 'PreviewCaspase3Raw.png'])
    
    imwrite(PreviewTH, [PreviewPath, filesep, IdentityString, 'PreviewTH.png'])
    imwrite(PreviewTHMask, [PreviewPath, filesep, IdentityString, 'PreviewTHMask.png'])
    imwrite(PreviewTHRaw, [PreviewPath, filesep, IdentityString, 'PreviewTHRaw.png'])
    
    imwrite(PreviewMAP2, [PreviewPath, filesep, IdentityString, 'PreviewMAP2.png'])
    imwrite(PreviewMAP2Mask, [PreviewPath, filesep, IdentityString, 'PreviewMAP2Mask.png'])
    imwrite(PreviewMAP2Raw, [PreviewPath, filesep, IdentityString, 'PreviewMAP2Raw.png'])
    
    imwrite(RGB, [PreviewPath, filesep, IdentityString, 'RGB.png'])
end

    
