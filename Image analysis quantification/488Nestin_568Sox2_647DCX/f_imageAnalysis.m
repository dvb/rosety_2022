function  [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, ch4, PreviewPath, MetaTable)
    %vol(ch1, 0, 5000) Hoechst
    %vol(ch2, 0, 4000) 647 DCX
    %vol(ch3, 0, 5000) 568 Sox2
    %vol(ch4, 0, 5000) 488 Nestin
    ObjectsThisOrganoid = table();
    %% Max projections
    %NucMax = max(ch1, [],3); imtool(NucMax,[])
    
    %% Segment Nuclei
%     NucDoG = imfilter(ch1, fspecial('gaussian', 21, 1) - fspecial('gaussian', 21, 3), 'symmetric'); % vol(NucDoG, 0, 100, 'hot')
%     NucMask = NucDoG > 100; %vol(NucMask, 0,1)

    ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 25, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 25, 5), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %figure; surf(fspecial('gaussian', 10, 5))
    ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch1DoG, 0, 2000, 'hot')
    NucleiMask = ch1DoG > 50; %vol(NucleiMask)
    %NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    NucleiMask = bwareaopen(NucleiMask, 50);%vol(NucleiMask)
    ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch3LP, 0, 3000, 'hot')
    NucMaskHigh =  (ch1LP > 2000) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucMaskAlive)
    %NucMask = ch1 > 3000; vol(NucMask, 0,1)
    
    %% Sox2
    Sox2DoG = imfilter(double(ch3), fspecial('gaussian', 101, 1) - fspecial('gaussian', 101, 21), 'symmetric');%vol(chSox2DoG, 0, 500, 'hot')
    Sox2Mask = Sox2DoG > 800; %vol(NucleiMask)
    NucDil = imdilate(imdilate(NucleiMask, strel('disk', 2)),strel('sphere',1));
    Sox2Mask= Sox2Mask & NucDil;
    Sox2Mask = imclose(squeeze(Sox2Mask), strel('disk', 5));
    Sox2LowMask = bwareaopen(Sox2Mask, 1000);%vol(Sox2Mask)
    
    Sox2HighMask = (Sox2DoG > 1800).* Sox2LowMask;
    Sox2Dil = imdilate(imdilate(Sox2LowMask, strel('disk', 4)), strel('sphere',1));
 clear('Sox2DoG')   

    %% Nestin
    
    for p=1:size(ch4, 3)
        ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch4_FT * 1000, 0, 100, hot)

    NestinMask = ch4_FT > 0.007;
    
    NestinMask = bwareaopen(NestinMask, 1500);%vol(DCXMask)

    
 clear('ch4_FT') 
     %% DCX

    for p=1:size(ch2, 3)
        ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [15,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [7,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch2_FT * 1000, 0, 100, hot)

    DCXMask = ch2_FT > 0.002;
    DCXMask = bwareaopen(DCXMask, 1000);
    DCXandNestin = DCXMask & NestinMask;
 clear('ch2_FT') 
 
 %% Colocalization masks
  Sox2andNestin = Sox2Dil & NestinMask;
  Sox2andDCX = Sox2Dil & DCXMask;
 %% DCX skeleton3D EPFL
    disp('Start skel')
    tic
    skelDCX = Skeleton3D(DCXMask);
    toc
    disp('Skel done')
    %vol(skelDCX, 0, 1)
    [AdjacencyMatrixDCX, nodeDCX, linkDCX] = Skel2Graph3D(skelDCX,0);                       
    %imtool(AdjacencyMatrixDCX, [])
    NodeDCX = zeros(size(DCXMask), 'uint8');
    NodeIdxs = vertcat(nodeDCX(:).idx);
    NodeDCX(NodeIdxs) = 1;
    %vol(NodeDCX)    
    if size(NodeIdxs, 1) == 0
        return
    end
    NodeDCXPreview = uint8(skelDCX) + NodeDCX + uint8(DCXMask); 
    NodeDCXPreview2D = max(NodeDCXPreview, [], 3);
    %it(NodeDCXPreview2D)
    %vol(NodeDCXPreview, 0, 3, 'jet')    
    NodeDegreeVectorDCX = sum(AdjacencyMatrixDCX, 1);

    ZeroNodeExplanationNeeded = 0;
    if ZeroNodeExplanationNeeded
        ZeroNodes = find(NodeDegreeVectorDCX == 0);
        ZeroNodesLinIdx = vertcat(nodeDCX(ZeroNodes).idx);
        ZeroNodeMask = zeros(size(DCXMaskClipped), 'uint8');
        ZeroNodeMask(ZeroNodesLinIdx) = 1; %vol(ZeroNodeMask)
        NodePreviewZeroCase = uint8(skelDCX) + NodeMaskDCX + 10*uint8(ZeroNodeMask) + uint8(DCXMask);
    end  

    %% DCX Fragmentation

    % Define structuring element for surface detection
    Conn6 = strel('sphere', 1); % 6 connectivity
    % Detect surface
    SurfaceDCX = DCXMask & ~(imerode(DCXMask, Conn6));
    %vol(SurfaceDCX)
    
    
    %% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
    Objects.Barcode = {MetaTable.Barcode};
    Objects.Batch = {MetaTable.Batch};
    Objects.Day = {MetaTable.Day};
    Objects.Sox2Mask = sum(Sox2LowMask(:));
	Objects.Sox2MFI = mean(ch3(Sox2LowMask));
    Objects.NestinMask = sum(NestinMask(:));
    Objects.DCXMask = sum(DCXMask(:));
    Objects.TotalNucMask = sum(NucleiMask(:));
	Objects.NucDeadMask = sum(NucMaskHigh(:));
	Objects.NestinByNucAlive = sum(NestinMask(:)) / sum(NucMaskAlive(:));
	Objects.DCXByNucAlive = sum(DCXMask(:)) / sum(NucMaskAlive(:));
	Objects.DCXandNestinByDCX = sum(DCXandNestin(:)) / sum(DCXMask(:));
    Objects.Sox2ByNucAlive = sum(Sox2LowMask(:)) / sum(NucMaskAlive(:));
    Objects.Sox2HighByNucAlive = sum(Sox2HighMask(:)) / sum(NucMaskAlive(:));
	Objects.SkelDCX = sum(skelDCX(:));
    Objects.NodesBySkel = size(nodeDCX, 2) / sum(skelDCX(:));
    Objects.LinksBySkel = size(linkDCX, 2)/ sum(skelDCX(:));
    Objects.DCXFragmentation = sum(SurfaceDCX(:)) / sum(DCXMask(:));
    Objects.Sox2andNestinBySox2 = sum(Sox2andNestin(:)) / sum(Sox2LowMask(:));
    Objects.Sox2andDCXBySox2 = sum(Sox2andDCX(:)) / sum(Sox2LowMask(:));
    
    
    %% Previews 
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');

    %RGB = cat(3, imadjust(max(chVimentin, [], 3), [0 0.3], [0 1]), imadjust(max(ch3, [], 3), [0 0.99], [0 1]), imadjust(max(ch1, [], 3), [0 0.3], [0 1]));
    %imtool(RGB)
    PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
    PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]); %imtool(PreviewHoechst)
    PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0.001 ; 0.08]));
    PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)
    PreviewNucDead = imoverlay2(imadjust(max(ch1,[],3),[0 0.1]), bwperim(max(NucMaskHigh,[],3)), [0 0 1]);
    PreviewNucDead = imoverlay2(PreviewNucDead, BarMask, [1 1 1]); %imtool(PreviewNucDead)
    PreviewNucDeadMask = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), max(NucMaskHigh,[],3), [0 0 1]); %imtool(PreviewNucDeadMask)
    PreviewNucAlive = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), bwperim(max(NucMaskAlive,[],3)), [0 0 1]);
    PreviewNucAlive = imoverlay2(PreviewNucAlive, BarMask, [1 1 1]); %imtool(PreviewNucAlive)
    PreviewNucAliveMask = imoverlay2(imadjust(max(ch1,[],3),[0 0.1]), max(NucMaskAlive,[],3), [0 0 1]); %imtool(PreviewNucAliveMask)
    
    PreviewDCX = imoverlay2(imadjust(max(ch2,[],3),[0 0.03]), bwperim(max(DCXMask,[],3)), [1 1 0]);
    PreviewDCX = imoverlay2(PreviewDCX, BarMask, [1 1 1]); %imtool(PreviewDCX)
    PreviewDCXMask = imoverlay2(imadjust(max(ch2,[],3),[0 0.03]), max(DCXMask,[],3), [1 1 0]); %imtool(PreviewDCXMask)
    PreviewDCXRaw = cat(3, imadjust(max(ch2, [], 3),[0.0012 ; 0.04]), imadjust(max(ch2, [], 3),[0.0012 ; 0.1]),max(chEmpty,[],3)); 
    PreviewDCXRaw = imoverlay2(PreviewDCXRaw, BarMask, [1 1 1]); %imtool(PreviewDCXRaw)
	
    PreviewSox2 = imoverlay2(imadjust(max(ch3,[],3),[0 0.15]), bwperim(max(Sox2LowMask,[],3)), [1 0 0]);
    PreviewSox2 = imoverlay2(PreviewSox2, BarMask, [1 1 1]); %imtool(PreviewSox2)
    PreviewSox2Mask = imoverlay2(imadjust(max(ch3,[],3),[0 0.15]), max(Sox2LowMask,[],3), [1 0 0]); %imtool(PreviewSox2Mask)
    PreviewSox2Raw = cat(3, imadjust(max(ch2, [], 3),[0.0001 ; 0.03]), imadjust(max(ch3, [], 3),[0.0001 ; 0.1]),max(chEmpty,[],3)); 
    PreviewSox2Raw = imoverlay2(PreviewSox2Raw, BarMask, [1 1 1]); %imtool(PreviewSox2Raw)
    
    PreviewSox2High = imoverlay2(imadjust(max(ch3,[],3),[0 0.15]), bwperim(max(Sox2HighMask,[],3)), [1 0 0]); %imtool(PreviewSox2High)
    PreviewSox2HighMask = imoverlay2(imadjust(max(ch3,[],3),[0 0.15]), max(Sox2HighMask,[],3), [1 0 0]);
    PreviewSox2HighMask = imoverlay2(PreviewSox2HighMask, BarMask, [1 1 1]); %imtool(PreviewSox2HighMask)
    
    PreviewNestin = imoverlay2(imadjust(max(ch4,[],3),[0 0.07]), bwperim(max(NestinMask,[],3)), [0 1 0]);
    PreviewNestin = imoverlay2(PreviewNestin, BarMask, [1 1 1]); %imtool(PreviewNestin)
    PreviewNestinMask = imoverlay2(imadjust(max(ch4,[],3),[0 0.3]), max(NestinMask,[],3), [0 1 0]); %imtool(PreviewNestinMask)
    PreviewNestinRaw = cat(3,max(chEmpty,[],3),imadjust(max(ch4, [], 3),[0.0012 ; 0.05]),max(chEmpty,[],3));
    PreviewNestinRaw  = imoverlay2(PreviewNestinRaw , BarMask, [1 1 1]); %imtool(PreviewNestinRaw)
    
    NestinHoechst = cat (3, max(chEmpty,[],3), imadjust(max(ch4, [], 3),[0.0012 ; 0.05]), imadjust(max(ch1, [], 3),[0.001 ; 0.08]));
    NestinHoechst = imoverlay2(NestinHoechst, BarMask, [1 1 1]); %imtool( NestinHoechst)
	
	    DCXHoechst = cat (3, imadjust(max(ch2, [], 3),[0.0012 ; 0.04]), imadjust(max(ch2, [], 3),[0.0012 ; 0.1]), imadjust(max(ch1, [], 3),[0.001 ; 0.08]));
    DCXHoechst = imoverlay2(DCXHoechst, BarMask, [1 1 1]); %imtool(DCXHoechst)
    
   
    RGB = cat (3, imadjust(max(ch2, [], 3),[0.0001 ; 0.05]), imadjust(max(ch4, [], 3),[0.0001 ; 0.3]), imadjust(max(ch1, [], 3),[0 ; 0.1]));
    RGB = imoverlay2(RGB, BarMask, [1 1 1]); %imtool(RGB)
    
    %% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '__', Objects.AreaName{:}{:}, '_']
    imwrite(PreviewHoechst, [PreviewPath, filesep, IdentityString, 'PreviewHoechst.png'])
    imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
    imwrite(PreviewNucDead, [PreviewPath, filesep, IdentityString, 'PreviewNucDead.png'])
    imwrite(PreviewNucAliveMask, [PreviewPath, filesep, IdentityString, 'PreviewNucAliveMask.png'])
    imwrite(PreviewNucDeadMask, [PreviewPath, filesep, IdentityString, 'PreviewNucDeadMask.png'])
    
    imwrite(PreviewNestin, [PreviewPath, filesep, IdentityString, 'PreviewNestin.png'])
    imwrite(PreviewNestinMask, [PreviewPath, filesep, IdentityString, 'PreviewNestinMask.png'])
    imwrite(PreviewNestinRaw, [PreviewPath, filesep, IdentityString, 'PreviewNestinRaw.png'])
    
    imwrite(PreviewDCX, [PreviewPath, filesep, IdentityString, 'PreviewDCX.png'])
    imwrite(PreviewDCXMask, [PreviewPath, filesep, IdentityString, 'PreviewDCXMask.png'])
    imwrite(PreviewDCXRaw, [PreviewPath, filesep, IdentityString, 'PreviewDCXRaw.png'])
    
    imwrite(PreviewSox2, [PreviewPath, filesep, IdentityString, 'PreviewSox2.png'])
    imwrite(PreviewSox2Mask, [PreviewPath, filesep, IdentityString, 'PreviewSox2Mask.png'])
    imwrite(PreviewSox2Raw, [PreviewPath, filesep, IdentityString, 'PreviewSox2Raw.png'])
    
    imwrite(PreviewSox2HighMask, [PreviewPath, filesep, IdentityString, 'PreviewSox2HighMask.png'])
 
    imwrite(DCXHoechst, [PreviewPath, filesep, IdentityString, 'NDCXHoechst.png'])
    imwrite(NestinHoechst, [PreviewPath, filesep, IdentityString, 'NestinHoechst.png'])
    imwrite(RGB, [PreviewPath, filesep, IdentityString, 'RGB.png'])
end

    
