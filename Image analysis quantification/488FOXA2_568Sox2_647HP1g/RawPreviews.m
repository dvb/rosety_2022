function  [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, ch4, PreviewPath, MetaTable)
   
    
    %% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
   
    
    %% Previews 
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');

    PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0 ; 0.1]));
    PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)
      
    PreviewHP1gRaw = cat(3, imadjust(max(ch2, [], 3),[0.001 ; 0.01]), imadjust(max(ch2, [], 3),[0.001 ; 0.02]),max(chEmpty,[],3)); 
    PreviewHP1gRaw = imoverlay2(PreviewHP1gRaw, BarMask, [1 1 1]); %imtool(PreviewHP1gRaw)
    PreviewHP1gRawGreen = cat(3, max(chEmpty,[],3),imadjust(max(ch2, [], 3),[0.0015 ; 0.015]), max(chEmpty,[],3)); 
    PreviewHP1gRawGreen = imoverlay2(PreviewHP1gRawGreen, BarMask, [1 1 1]); %imtool(PreviewHP1gRawGreen)
    
    
    PreviewSox2Raw = cat(3, imadjust(max(ch3, [], 3),[0.008 ; 0.08]),max(chEmpty,[],3), max(chEmpty,[],3)); 
    PreviewSox2Raw = imoverlay2(PreviewSox2Raw, BarMask, [1 1 1]); %imtool(PreviewSox2Raw)
    
    
    PreviewFOXA2Raw = cat(3,max(chEmpty,[],3),imadjust(max(ch4, [], 3),[0.005 ; 0.02]),max(chEmpty,[],3));
    PreviewFOXA2Raw  = imoverlay2(PreviewFOXA2Raw , BarMask, [1 1 1]); %imtool(PreviewFOXA2Raw)
    

    RGB_FOXA2_Sox2 = cat (3, imadjust(max(ch3,[],3),[0.008 ; 0.06 ]), imadjust(max(ch4,[],3),[0.005 ; 0.02]),max(chEmpty,[],3));
    RGB_FOXA2_Sox2  = imoverlay2(RGB_FOXA2_Sox2, BarMask, [1 1 1]); %imtool(RGB_FOXA2_Hoechst)
    
    RGB_HP1g_Sox2 = cat (3, imadjust(max(ch3,[],3),[0.008 ; 0.08 ]), imadjust(max(ch2,[],3),[0.0015 ; 0.015]),max(chEmpty,[],3));
    RGB_HP1g_Sox2   = imoverlay2(RGB_HP1g_Sox2 , BarMask, [1 1 1]); %imtool(RGB_HP1g_Sox2)
    
    
  
    %% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '_', Objects.AreaName{:}{:}, '_']
    imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
    imwrite(PreviewFOXA2Raw, [PreviewPath, filesep, IdentityString, 'PreviewFOXA2Raw.png'])
    
    imwrite(PreviewHP1gRaw, [PreviewPath, filesep, IdentityString, 'PreviewHP1gRaw.png'])
    imwrite(PreviewHP1gRawGreen, [PreviewPath, filesep, IdentityString, 'PreviewHP1gRawGreen.png'])
    imwrite(PreviewSox2Raw, [PreviewPath, filesep, IdentityString, 'PreviewSox2Raw.png'])
    
    imwrite(RGB_HP1g_Sox2, [PreviewPath, filesep, IdentityString, 'RGB_HP1g_Sox2.png'])
    imwrite(RGB_FOXA2_Sox2, [PreviewPath, filesep, IdentityString, 'RGB_FOXA2_Sox2.png'])
end

    
