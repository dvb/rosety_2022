function  [Objects] = f_imageAnalysis(Label, ch1, ch2, ch3, ch4, PreviewPath, MetaTable)
    %vol(ch1, 0, 5000) Hoechst
    %vol(ch2, 0, 4000) 647 MAP2
    %vol(ch3, 0, 5000) 568 TotSyn
    %vol(ch4, 0, 5000) 488 pSyn
    ObjectsThisOrganoid = table();
    %% Max projections
    %NucMax = max(ch1, [],3); imtool(NucMax,[])
    
    %% Segment Nuclei
%     NucDoG = imfilter(ch1, fspecial('gaussian', 21, 1) - fspecial('gaussian', 21, 3), 'symmetric'); % vol(NucDoG, 0, 100, 'hot')
%     NucMask = NucDoG > 100; %vol(NucMask, 0,1)

    ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 25, 1), 'symmetric');%vol(ch3BlurSmall)
    ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 25, 5), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %figure; surf(fspecial('gaussian', 10, 5))
    ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch1DoG, 0, 2000, 'hot')
    NucleiMask = ch1DoG > 50; %vol(NucleiMask)
    %NucleiMask = imdilate(imdilate(NucleiMask, strel('disk', 1)), strel('sphere',1));
    NucleiMask = bwareaopen(NucleiMask, 50);%vol(NucleiMask)
    ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch3LP, 0, 3000, 'hot')
    NucMaskHigh =  (ch1LP > 2000) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucMaskAlive)
    %NucMask = ch1 > 3000; vol(NucMask, 0,1)
    
    %% TotSyn
    for p=1:size(ch3, 3)
        ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [15,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch4_FT * 1000, 0, 100, hot)

    TotSynMask = ch3_FT > 0.005;
    
    TotSynMask = bwareaopen(TotSynMask, 100);%vol(MAP2Mask)

    
    %TotSynHighMask = (TotSynDoG > 1800).* TotSynLowMask;

 clear('ch3_FT')   

    %% pSyn
    
    for p=1:size(ch4, 3)
        ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
        %ch4_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [7,1], 0);
        %ch3_FT(:,:,p) = f_LPF_by_FFT(ch3(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch4_FT * 1000, 0, 100, hot)

    pSynMask = ch4_FT > 0.007;
    
    pSynMask = bwareaopen(pSynMask, 100);%vol(MAP2Mask)

    
 clear('ch4_FT') 
     %% MAP2

    for p=1:size(ch2, 3)
        ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [15,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [7,1], 0);
        %ch2_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [31,1], 0); %for bigger structures like masks only on cell body (total synuclein)
    end % vol(ch2_FT * 1000, 0, 100, hot)

    MAP2Mask = ch2_FT > 0.01;
    MAP2Mask = bwareaopen(MAP2Mask, 100);
    
    
    clear('ch2_FT')  
    %% Colocalization Masks
    TotSynandpSyn = TotSynMask & pSynMask;
    NeuronalSyn = MAP2Mask & TotSynMask;
    NeuronalpSyn= MAP2Mask & pSynMask;

    
    %% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = {MetaTable.Idx};
    Objects.Well = {MetaTable.Well};
    Objects.AreaName = {MetaTable.AreaName};
    Objects.OrganoidID = {MetaTable.OrganoidID};
    Objects.Barcode = {MetaTable.Barcode};
    Objects.Batch = {MetaTable.Batch};
    Objects.Day = {MetaTable.Day};
    Objects.TotSynMask = sum(TotSynMask(:));
	Objects.TotSynMFI = mean(ch3(TotSynMask));
    Objects.pSynMask = sum(pSynMask(:));
    Objects.MAP2Mask = sum(pSynMask(:));
    Objects.TotalNucMask = sum(NucleiMask(:));
	Objects.NucDeadMask = sum(NucMaskHigh(:));
	Objects.pSynByNucAlive = sum(pSynMask(:)) / sum(NucMaskAlive(:));
	Objects.MAP2ByNucAlive = sum(MAP2Mask(:)) / sum(NucMaskAlive(:));
    Objects.TotSynByNucAlive = sum(TotSynMask(:)) / sum(NucMaskAlive(:));
    Objects.TotSynandpSynByTotSyn = sum(TotSynandpSyn(:)) / sum(TotSynMask(:));
    Objects.NeuronalSyn= sum(NeuronalSyn(:)) / sum(MAP2Mask(:));
    Objects.NeuronalpSyn= sum(NeuronalpSyn(:)) / sum(MAP2Mask(:));
    
    
    %% Previews 
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    chEmpty = zeros(size(ch1),'uint16');

    %RGB = cat(3, imadjust(max(chVimentin, [], 3), [0 0.3], [0 1]), imadjust(max(ch3, [], 3), [0 0.99], [0 1]), imadjust(max(ch1, [], 3), [0 0.3], [0 1]));
    %imtool(RGB)
    PreviewHoechst = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), bwperim(max(NucleiMask,[],3)), [0 0 1]);
    PreviewHoechst = imoverlay2(PreviewHoechst, BarMask, [1 1 1]); %imtool(PreviewHoechst)
    PreviewHoechstRaw = cat(3,max(chEmpty,[],3), max(chEmpty,[],3), imadjust(max(ch1, [], 3),[0 ; 0.07]));
    PreviewHoechstRaw = imoverlay2(PreviewHoechstRaw, BarMask, [1 1 1]);%imtool(PreviewHoechstRaw)
    PreviewNucDead = imoverlay2(imadjust(max(ch1,[],3),[0 0.1]), bwperim(max(NucMaskHigh,[],3)), [0 0 1]);
    PreviewNucDead = imoverlay2(PreviewNucDead, BarMask, [1 1 1]); %imtool(PreviewNucDead)
    PreviewNucDeadMask = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), max(NucMaskHigh,[],3), [0 0 1]); %imtool(PreviewNucDeadMask)
    PreviewNucAlive = imoverlay2(imadjust(max(ch1,[],3),[0 0.07]), bwperim(max(NucMaskAlive,[],3)), [0 0 1]);
    PreviewNucAlive = imoverlay2(PreviewNucAlive, BarMask, [1 1 1]); %imtool(PreviewNucAlive)
    PreviewNucAliveMask = imoverlay2(imadjust(max(ch1,[],3),[0 0.1]), max(NucMaskAlive,[],3), [0 0 1]); %imtool(PreviewNucAliveMask)
    
    PreviewMAP2 = imoverlay2(imadjust(max(ch2,[],3),[0 0.07]), bwperim(max(MAP2Mask,[],3)), [1 1 0]);
    PreviewMAP2 = imoverlay2(PreviewMAP2, BarMask, [1 1 1]); %imtool(PreviewMAP2)
    PreviewMAP2Mask = imoverlay2(imadjust(max(ch2,[],3),[0 0.07]), max(MAP2Mask,[],3), [1 1 0]); %imtool(PreviewMAP2Mask)
    PreviewMAP2Raw = cat(3, imadjust(max(ch2, [], 3),[0.0001 ; 0.07]), imadjust(max(ch2, [], 3),[0.0001 ; 0.1]),max(chEmpty,[],3)); 
    PreviewMAP2Raw = imoverlay2(PreviewMAP2Raw, BarMask, [1 1 1]); %imtool(PreviewMAP2Raw)
    
    PreviewTotSyn = imoverlay2(imadjust(max(ch3,[],3),[0 0.05]), bwperim(max(TotSynMask,[],3)), [1 0 0]);
    PreviewTotSyn = imoverlay2(PreviewTotSyn, BarMask, [1 1 1]); %imtool(PreviewTotSyn)
    PreviewTotSynMask = imoverlay2(imadjust(max(ch3,[],3),[0 0.05]), max(TotSynMask,[],3), [1 0 0]); %imtool(PreviewTotSynMask)
    PreviewTotSynRaw = cat(3, imadjust(max(ch3, [], 3),[0.0001 ; 0.05]),max(chEmpty,[],3), max(chEmpty,[],3)); 
    PreviewTotSynRaw = imoverlay2(PreviewTotSynRaw, BarMask, [1 1 1]); %imtool(PreviewTotSynRaw)
    
 
    PreviewpSyn = imoverlay2(imadjust(max(ch4,[],3),[0 0.07]), bwperim(max(pSynMask,[],3)), [0 1 0]);
    PreviewpSyn = imoverlay2(PreviewpSyn, BarMask, [1 1 1]); %imtool(PreviewpSyn)
    PreviewpSynMask = imoverlay2(imadjust(max(ch4,[],3),[0 0.3]), max(pSynMask,[],3), [0 1 0]); %imtool(PreviewpSynMask)
    PreviewpSynRaw = cat(3,max(chEmpty,[],3),imadjust(max(ch4, [], 3),[0.0001 ; 0.1]),max(chEmpty,[],3));
    PreviewpSynRaw  = imoverlay2(PreviewpSynRaw , BarMask, [1 1 1]); %imtool(PreviewpSynRaw)
    

    RGB = cat (3, imadjust(max(ch2, [], 3),[0.0001 ; 0.05]), imadjust(max(ch4, [], 3),[0.0001 ; 0.3]), imadjust(max(ch1, [], 3),[0 ; 0.1]));
    RGB = imoverlay2(RGB, BarMask, [1 1 1]); %imtool(RGB)
    
    %% Write Previews 
    %IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}]
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}{:}, '__', Objects.AreaName{:}{:}, '_']
    imwrite(PreviewHoechst, [PreviewPath, filesep, IdentityString, 'PreviewHoechst.png'])
    imwrite(PreviewHoechstRaw, [PreviewPath, filesep, IdentityString, 'PreviewHoechstRaw.png'])
    imwrite(PreviewNucDead, [PreviewPath, filesep, IdentityString, 'PreviewNucDead.png'])
    imwrite(PreviewNucAliveMask, [PreviewPath, filesep, IdentityString, 'PreviewNucAliveMask.png'])
    imwrite(PreviewNucDeadMask, [PreviewPath, filesep, IdentityString, 'PreviewNucDeadMask.png'])
    
    imwrite(PreviewpSyn, [PreviewPath, filesep, IdentityString, 'PreviewpSyn.png'])
    imwrite(PreviewpSynMask, [PreviewPath, filesep, IdentityString, 'PreviewpSynMask.png'])
    imwrite(PreviewpSynRaw, [PreviewPath, filesep, IdentityString, 'PreviewpSynRaw.png'])
    
    imwrite(PreviewMAP2, [PreviewPath, filesep, IdentityString, 'PreviewMAP2.png'])
    imwrite(PreviewMAP2Mask, [PreviewPath, filesep, IdentityString, 'PreviewMAP2Mask.png'])
    imwrite(PreviewMAP2Raw, [PreviewPath, filesep, IdentityString, 'PreviewMAP2Raw.png'])
    
    imwrite(PreviewTotSyn, [PreviewPath, filesep, IdentityString, 'PreviewTotSyn.png'])
    imwrite(PreviewTotSynMask, [PreviewPath, filesep, IdentityString, 'PreviewTotSynMask.png'])
    imwrite(PreviewTotSynRaw, [PreviewPath, filesep, IdentityString, 'PreviewTotSynRaw.png'])
    
    %imwrite(PreviewTotSynHighMask, [PreviewPath, filesep, IdentityString, 'PreviewTotSynHighMask.png'])
 
    
    imwrite(RGB, [PreviewPath, filesep, IdentityString, 'RGB.png'])
end

    
