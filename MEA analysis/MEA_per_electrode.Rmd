---
title: "nalysis of MEA data and plotting - using output from matlab"
output: html_notebook
---


```{r}
library(ggplot2)
# library(RColorBrewer)
library(tidyverse)
library(ggpubr)
library(jcolors)
library(dplyr)
library(janitor)
library(plotrix)
```


```{r}

# load data 
# in excel pre generate a file with only including well information / R error when duplicated row names 

setwd("//atlas.uni.lux/users/isabel.rosety/GBA/MEA/RstudioAnalysis/")


# NOTE: before and during loading, re-check the index of the rows. it is variable in some files (not fix index between extracted files)


#Day 15
d15_1<- read.csv("//atlas.uni.lux/LCSB_Cellular_Biology/16-Our Papers/In Preparation/GBA hMO_Isabel/Figures/Fig4/Originals/H/MEA recordings/E40D15/Neural_Statistics_Compiler_20210803_E40d15.csv",header = T, skip = 157, sep=";",     nrows = 26)
n <- d15_1$Measurement
d15_1t <- as.data.frame(t(d15_1[,-1])) 
colnames( d15_1t) <- n  
m <- rownames(d15_1t) 
d15_1t %>% mutate(wells = rownames(d15_1t)) %>% mutate(timePoint = "day15") %>% mutate(day = 15) %>% mutate(experiment = "exp40") ->  d15_1t
m -> rownames(d15_1t)

d15_2<- read.csv("//atlas.uni.lux/LCSB_Cellular_Biology/16-Our Papers/In Preparation/GBA hMO_Isabel/Figures/Fig4/Originals/H/MEA recordings/E41D15/20210806_E41_D15(000).csv",header = T, skip = 157, sep=";",     nrows = 26)
n <- d15_2$Measurement
d15_2t <- as.data.frame(t(d15_2[,-1])) 
colnames( d15_2t) <- n  
m <- rownames(d15_2t) 
d15_2t %>% mutate(wells = rownames(d15_2t)) %>% mutate(timePoint = "day15") %>% mutate(day = 15) %>% mutate(experiment = "exp41") ->  d15_2t
m -> rownames(d15_2t)

d15_3<- read.csv("//atlas.uni.lux/LCSB_Cellular_Biology/16-Our Papers/In Preparation/GBA hMO_Isabel/Figures/Fig4/Originals/H/MEA recordings/E42D15/20210817_E42D15(000).csv",header = T, skip = 157, sep=";",     nrows = 26)
n <- d15_3$Measurement
d15_3t <- as.data.frame(t(d15_3[,-1])) 
colnames( d15_3t) <- n  
m <- rownames(d15_3t) 
d15_3t %>% mutate(wells = rownames(d15_3t)) %>% mutate(timePoint = "day15") %>% mutate(day = 15) %>% mutate(experiment = "exp42") ->  d15_3t
m -> rownames(d15_3t)

d15_4<- read.csv("//atlas.uni.lux/LCSB_Cellular_Biology/16-Our Papers/In Preparation/GBA hMO_Isabel/Figures/Fig4/Originals/H/MEA recordings/E43D15/20210820_E43_D15(001).csv",header = T, skip = 157, sep=";",     nrows = 26)
n <- d15_4$Measurement
d15_4t <- as.data.frame(t(d15_4[,-1])) 
colnames( d15_4t) <- n  
m <- rownames(d15_4t) 
d15_4t %>% mutate(wells = rownames(d15_4t)) %>% mutate(timePoint = "day15") %>% mutate(day = 15) %>% mutate(experiment = "exp43") ->  d15_4t
m -> rownames(d15_4t)  
 
 
```

Merge data. obtain sumary data file (over threshold and without threshold) 
```{r}

# cahnge folder for results 
setwd("//atlas.uni.lux/users/isabel.rosety/GBA/MEA/RstudioAnalysis/")

 ## merge all files form different time points 
dataWell <- rbind(d15_1t,d15_2t,d15_3t,d15_4t)

#dataWell <- rbind(d15_1t,d15_2t,d15_3t,d15_4t)
#dataWell[dataWell==0] <- NA
head(dataWell)

# add conditions to wells 


colnames(dataWell)<-c( 'NumberOfSpikes', 'MeanFiringRate', 'ISI', 'NumberOfBurst','BurstDuration_avrg', 'BurstDuration_std', 'NumberSpikesPerBurst_avg', 'NumberSpikesPerBurst_std', 'meanISI_burst_avg','meanISI_burst_std','medianISI_burst_avg','medianISI_burst_std','InterBurstInterval_avg', 'InterBurstInterval_std', 'BurstFrequency', 'IBI_CoefficientOfVariation', 'Normalized_Duration_IQR','BurstPercentage', 'condition','CellLine','wells', 'timepoint', 'day','experiment')

# create colum for removal of outliers (per condition & timepoint)
dataWell %>% mutate(out = paste(dataWell$condition, dataWell$timepoint)) ->  dataWell #here we create a new column called out, with condition and timepoint, because outliers will be calculated based on condition and timepoint

# convert into numeric all data of interest fro plotting (needed because data was as character) 
dataWell$NumberOfSpikes <- as.numeric(as.character(dataWell$NumberOfSpikes))
dataWell$MeanFiringRate <- as.numeric(as.character(dataWell$MeanFiringRate))
dataWell$ISI <- as.numeric(as.character(dataWell$ISI))
dataWell$NumberOfBurst <- as.numeric(as.character(dataWell$NumberOfBurst))
dataWell$BurstDuration_avrg <- as.numeric(as.character(dataWell$BurstDuration_avrg))
dataWell$BurstDuration_std <- as.numeric(as.character(dataWell$BurstDuration_std))
dataWell$NumberSpikesPerBurst_avg <- as.numeric(as.character(dataWell$NumberSpikesPerBurst_avg))
dataWell$NumberSpikesPerBurst_std <- as.numeric(as.character(dataWell$NumberSpikesPerBurst_std))
dataWell$meanISI_burst_avg <- as.numeric(as.character(dataWell$meanISI_burst_avg))
dataWell$meanISI_burst_std <- as.numeric(as.character(dataWell$meanISI_burst_std))
dataWell$medianISI_burst_std <- as.numeric(as.character(dataWell$medianISI_burst_std))
dataWell$medianISI_burst_avg <- as.numeric(as.character(dataWell$medianISI_burst_avg))
dataWell$InterBurstInterval_avg <- as.numeric(as.character(dataWell$InterBurstInterval_avg))
dataWell$InterBurstInterval_std <- as.numeric(as.character(dataWell$InterBurstInterval_std))
dataWell$BurstFrequency <- as.numeric(as.character(dataWell$BurstFrequency))
dataWell$IBI_CoefficientOfVariation <- as.numeric(as.character(dataWell$IBI_CoefficientOfVariation))
dataWell$Normalized_Duration_IQR <- as.numeric(as.character(dataWell$Normalized_Duration_IQR))
dataWell$BurstPercentage <- as.numeric(as.character(dataWell$BurstPercentage))
dataWell$day <- as.numeric(as.character(dataWell$day))


# obtain data only with bursting information for extracting into CSV and plotting in graphad (needed to take into consideration SD already calculated by software)
dataWell_burst <- dataWell[dataWell$NumberOfBurst != 0, ] #remove wells with no information for bursting 

dataWell_5spike <- dataWell[dataWell$NumberOfSpikes >= 5, ] #this is to select the measurements with more than 5 spikes per minute
dataWell_5spike_burst <- dataWell_burst[dataWell_burst$NumberOfSpikes >= 5, ]
dataWell_5spike<-filter(dataWell_5spike, condition %in% c("Control","PD_N370S"))

feature_names <- colnames(dataWell_5spike[,c(1:4,15:18)])  
#saving info: 

  #save summary file in CSV
  write.csv(dataWell,file="dataWell_allInfo.csv")
  write.csv(dataWell_5spike,file="dataWell_5spike.csv")
  # save information reagrding average and std
  write.csv(dataWell_burst,file="dataWell_burst_allInfo.csv") 
  write.csv(dataWell_5spike_burst,file="dataWell_5spike_burst.csv")


```

Remove outliers of any data points which lie beyond the extremes of the whiskers(for only the specific feature under study_ introducetion NAs) thes rest of the information for the full well remains 
```{r} 
# NOTE!! if zeros are kept, may infomation will be extracted based on a simple value due to the ceros being the mean !! 
# transfrom 0 into NAs 

#in this one it wont remove the entire electrode, it will only remove the value of the outlier feature

dataWell_noZero <- replace(dataWell_5spike, dataWell_5spike == 0, NA)
#dataWell_noZero <- replace(dataWell, dataWell == 0, NA)

 groupOut <- unique(dataWell_noZero$out) # groupout 
datatest_NA <- data.frame()


for (i in 1:length(groupOut))
{
  datatest <- dataWell_noZero %>% 
  filter(out %in% groupOut[i]) # go for each line and extract rows for that line 
  for (j in 1:18) # for (j in 2:length(exprs))
  {
    out <- boxplot.stats(datatest[,j])$out
    for (k in 1:length(out))
    {
    datatest[,j][datatest[,j] == out[k]] <- NA #it changes the outlier value for NA
    }
    rm(out)
  }
  datatest_NA <- rbind(datatest_NA,datatest)
  rm(datatest)
} 


#saving info: 
# files without outliers were generated from file No zeros - reason: too many zeros cause exclusion of real values as outliers 
# _well > meaningn all well information was removed for all variable when at least one of the variables presented one outlier -> the well is not consider any longer 

# Remove an entire row if it has >22 NAs (for example >50% of your features)
count_na <- function(x)sum(is.na(x))
data_noOut_value <- datatest_NA %>%
  dplyr::mutate(count_na = apply(., 1, count_na))
data_noOut_value <-datatest_NA[!(data_noOut_value$count_na >= 19),] # remove wells containing no info 

dataWell_burst_NoOut_value <- subset(data_noOut_value, !is.na(NumberOfBurst)) #remove wells with no information for bursting

data_noOut_value %>% 
   mutate(CellLine = str_replace_all(CellLine, 
            pattern = "WT 56", replacement = "CTRL1")) %>%
    mutate(CellLine = str_replace_all(CellLine, 
            pattern = "WT 39", replacement = "CTRL2"))  %>%
    mutate(CellLine = str_replace_all(CellLine, 
            pattern = "WT 68", replacement = "CTRL3")) %>%
    mutate(CellLine = str_replace_all(CellLine, 
            pattern = "MUT 309", replacement = "PD1")) %>%
    mutate(CellLine= str_replace_all(CellLine, 
            pattern = "MUT KTI6", replacement = "PD2")) %>%
    mutate(CellLine = str_replace_all(CellLine, 
            pattern = "MUT SGO1", replacement = "PD3")) ->data_noOut_value

  write.csv(dataWell_burst_NoOut_value,file="dataWell_burst_NoOut_value.csv") # info for bursting (graphad) _ only the specific outlier was removed. well information for other variables remain  
  write.csv(data_noOut_value,file="data_noOut_value.csv") # infor - no outliers for specific variable -well info remains 
  

feature_names <- colnames(data_noOut_value[,c(1:4,15:18)])  
head(data_noOut_value)
  
```

Shapiro Test to check normality
# plotting without SD already calculated ("NumberOfSpikes" "MeanFiringRate"  "ISI"  "NumberOfBurst"  "NumberBurstingElectrodes")
```{r}
#if p-value >0.05, normality can be assumed
TestShapiro<- select(data_noOut_value, c(1:18))
#shapiro.test(data_noOut_value$MeanFiringRate)
apply(TestShapiro,2,shapiro.test)

```

Violin Plots
```{r warning=FALSE}

names <- c("Number Of Spikes", "Mean Firing Rate (spikes/s)" , "Interspike Interval [ISI]"  ,"Number Of Bursts" , "Burst Frequency (bursts/s)","IBI Coefficient Of Variation", "Normalized Duration IQR"  ,  "Burst Percentage")

for (i in 1:length(feature_names)) {
data_noOut_value  %>% #change dataset selected - change saving option depending on the dataset 
 pivot_longer(cols=feature_names, names_to = "feature", values_to = "value") %>%
  filter(feature %in% feature_names[i]) %>%
  ggplot( aes(x=factor(condition, level = c('Control','PD_N370S')), y=value)) +
  #geom_violin( aes(fill=Condition),show.legend = T, trim=T),
  geom_violin( aes(fill=condition),show.legend = T,scale = "width", trim=F)+
  geom_dotplot(binaxis = "y",stackdir = "center",dotsize=0.8)+
    scale_fill_manual(values= c("#FFFFFF","#999999"),name = "Condition", guide = FALSE)+
    #scale_fill_manual(values= alpha(c("#1565C0","#CC0000"),0.75),name = "Condition",guide = "none")+
    #scale_fill_manual(values= c("white", "black"),name = "condition", guide = "none") +  
    #geom_point(aes(color=Batch),shape=18, size=3,show.legend = T)+
    #scale_color_manual(values = rev(brewer.pal(n=6, name="OrRd")))+
    scale_color_jcolors("pal7")+
    #scale_color_viridis(option = "D", discrete=TRUE)+
    #geom_point(shape = 1,size = 3,colour = "black")+
    theme(legend.key=element_blank()) +
    
   geom_signif(comparisons = list(c('Control','PD_N370S')), test='wilcox.test',
              vjust=0.6, size=0.5, margin_top=0.5, textsize=9, map_signif_level=c("****"=0.0001,"***"=0.001, "**"=0.01, "*"=0.05,  " "=2) ) +
  #facet_grid(~fct_relevel(Day, "d30","d60"), scales="free") +
      labs(x     = "",
       y     = paste(feature_names[i]),
       #y     = paste(names[i]),
       fill  = "Condition",
       #title = paste(feature_names[i])) +
       title = "" )+
  theme_bw() +
  theme(
    axis.line = element_line(colour = 'black', size = 0.5) ,
    axis.title.x = element_blank(),
    axis.text.x = element_text(size=16, color="black"),
    axis.title.y = element_text(size = 16),
    axis.text.y = element_text(size=10, color="black"),
    axis.ticks.y = element_line(),
    axis.ticks.length=unit(.25, "cm"),
    #change legend text font size)
    #legend.key.size = unit(0.7, "cm"),
    #legend.key.width = unit(0.6,"cm"),
    legend.key=element_blank(),
    panel.grid.major = element_blank(), 
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    plot.title = element_text(size = 20, hjust=0.5, vjust= 1, face = "bold"),
    plot.subtitle = element_blank(),#element_text(size = 2, hjust=0.5)
    strip.text = element_text(size=12, vjust=0.5),
    strip.background = element_rect(fill="lightgray"),
   # panel.border = element_rect(fill = NA, color = "black"),
    panel.spacing.y = unit(0.8, "lines"),
    strip.switch.pad.wrap=unit(20, "lines"),
    legend.position="right",
    legend.text = element_text(size=17),
    legend.title = element_text(size=19)
    
  )  -> p
  #t<- cowplot::ggdraw(cowplot::add_sub(p, "Wilcox-test, ***p=0.001, **p=0.01, *p=0.05",hjust=-0.2, size=13))
  print(p)
  ##ggsave(paste0(Sys.Date(),"_", names[i], ".pdf"), plot=t)
#ggsave(paste0(Sys.Date(),(sprintf(" ViolinPlot_%s_DIV15.pdf",feature_names[i]))),height=3.5,width=3)
}

```


