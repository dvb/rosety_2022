---
title: "R Notebook"
output: html_notebook
---


```{r}

## gene level analysis of transcriptomic data starting from foldchage values 

## set path and load file 

library(readxl)
library(dplyr)
library(tidyverse)
library(tidyr)
library(ggplot2) 
suppressPackageStartupMessages(library(openxlsx))
library(readxl)
library(limma)
library(edgeR)
suppressPackageStartupMessages(library(clusterProfiler))
suppressPackageStartupMessages(library(org.Hs.eg.db))
library(lubridate)
library(extrafont)
library(showtext)
library(multicolor)
font_import()
loadfonts(device = "win")

suppressPackageStartupMessages(library(ComplexHeatmap))

```

```{r}
#setwd("//atlas.uni.lux/users/isabel.rosety/GBA/RNAseq/RNAseq_June2021/R script for transcriptomic data/scripts and sample R data")
setwd("//atlas.uni.lux/LCSB_Cellular_Biology/16-Our Papers/In Preparation/GBA hMO_Isabel/Figures/Supplementary Figures/Fig S4/D/Originals")
getwd() 

data <- read_excel("//atlas.uni.lux/LCSB_Cellular_Biology/16-Our Papers/In Preparation/GBA hMO_Isabel/Figures/Supplementary Figures/Fig S4//D/Originals/gba_vs_wt_meta-analysis_04_07-2021_deseq2_ranking.xlsx",sheet = 2) 
#in sheet 2 there are the values with FDR<0.05
#View(gba_mutant_vs_wildtype_rsubread_edger_ranking_TranscriptRemoved)
head(data)

```

```{r}
# Kinases and phosphatases of PI signaling

PI_signaling <- read_excel("//atlas.uni.lux/LCSB_Cellular_Biology/16-Our Papers/In Preparation/GBA hMO_Isabel/Figures/Supplementary Figures/Fig S4/D/Originals/List of PI signaling genes/Kinases_phosphatases in PI signaling_Data compiled from BrainRNAseq.xlsx",sheet=2)
#View(pathcards_Dopaminergic_neurogenesis_genes)
target <- c(intersect(PI_signaling$Gene_Symbol, data$Gene_Symbol))
PI_signaling_FDR <- filter(data, Gene_Symbol %in% target)
rm(target)
#write.csv(PI_signaling_FDR, file = 'DEGs_PI_signaling_kinases_phosphatases_FDR.csv')


#PIP binding proteins

PIP_BindingProteins <- read_excel("//atlas.uni.lux/LCSB_Cellular_Biology/16-Our Papers/In Preparation/GBA hMO_Isabel/Figures/Supplementary Figures/Fig S4/D/Originals/List of PI signaling genes/phosphoinositide binding proteins.xlsx")

target <- c(intersect(PIP_BindingProteins$Gene_Symbol, data$Gene_Symbol))
PIP_BindingProteins_FDR <- filter(data, Gene_Symbol %in% target)
rm(target)

#write.csv(PIP_BindingProteins_FDR, file = 'DEGs_PIP_BindingProteins_FDR.csv')
```

Plotting
```{r}

    t <- ggplot(PI_signaling_FDR, aes(x = reorder(Gene_Symbol,MetaFDR) , y = MeanLogFC))+ #y values are ordered according to FDR values
    #t <- ggplot(PIP_BindingProteins, aes(x = reorder(Gene_Symbol,MetaFDR) , y = MeanLogFC))+ #y values are ordered according to FDR values
      geom_col(aes(fill = MetaFDR), width = 0.7)+
      labs(x     ="Genes",
           y     = "LogFC",
           #title = "Enrichment analysis \n by process networks") 
           title = "") +
      theme(
        axis.line = element_line(colour = 'black', size = 0.1) ,
        axis.title.x = element_text(size = 18),
        axis.text.x = element_text(size=7, color="black",hjust=1,vjust=0.5, angle=90),
        axis.title.y = element_text(size = 18),
        axis.text.y = element_text(size=15, color="black"),
        axis.ticks.y = element_line(),
        axis.ticks.length=unit(.25, "cm"),
        #change legend text font size)
        legend.key.size = unit(0.7, "cm"),
        legend.key.width = unit(0.6,"cm"),
        
        plot.title = element_text(size = 20, hjust=0.5, vjust= 1, face = "bold"),
        plot.subtitle = element_blank(),#element_text(size = 2, hjust=0.5)
        strip.text = element_text(size=12, vjust=0.5),
        panel.background = element_blank(),
        #panel.border = element_rect(fill = NA, color = "black"),
        panel.spacing.y = unit(0.8, "lines"),
        strip.switch.pad.wrap=unit(20, "lines"),
        legend.position="right",
        legend.text = element_text(size=15),
        legend.title = element_text(size=18),
      )
    
    t
    
    #ggsave(paste0(Sys.Date()," PI signaling DEGs.pdf"), width = 6, height=8)
    #ggsave(paste0(Sys.Date()," PIP_BindingProteins DEGs.pdf"))

```


Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
