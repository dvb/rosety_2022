---
title: "R Notebook"
output: html_notebook
---

```{r}
library(ggplot2)
library(pheatmap)
library(RColorBrewer)
#display.brewer.all()
library(tidyverse)
library(ggpubr)
library(ggsignif)
library(openxlsx)
library(viridis)
library(data.table)
library(readxl)
library(janitor)
library(dplyr)
library(jcolors)
library(stringr)
```

```{r}

setwd("//atlas.uni.lux/users/isabel.rosety/GBA/Stainings/Plots/")
#dir(path="//atlas.uni.lux/users/isabel.rosety/GBA/Organoids with Matrix/Stainings/Plots/488TUJ1_568LMNB1_Sox2647",full.names = TRUE) %>%
#  map(~fread(.,fill = TRUE)) -> mytables #fread needs the whole path
#  data = bind_rows(mytables) 
  
#data = dplyr::bind_rows(mytables)

data<-read.csv("//atlas.uni.lux/users/isabel.rosety/GBA/Stainings/Plots/20211209_488LAMP1_568GCase_647MAP2Analysis_20211214_170530/20211209_488LAMP1_568GCase_647MAP2Analysis_20211214_170530.csv",sep=";")


#tmp=do.call(rbind,strsplit(data$AreaName,"_")) #stringsplit will take the column Areaname and will split this depending on the underscore, then we put them otgeter one ofter the other

#tmp is a matrix, it is neither a dataframe nor a table, in a table you have to have column names, adding a row in a matrix is easier than in a table
#some areanames 2 underscores, others have 4 underscores, the number of maximum underscores defines the number of columns, so when only 2 underscores, it will fill the other 2 columns by repeating the value

#data$Condition = tmp[,1]

data %>% 
   mutate(Condition = str_replace_all(Condition, 
            pattern = "MUT", replacement = "GBA-PD")) %>%
     mutate(Condition = str_replace_all(Condition, 
            pattern = "WT", replacement = "CTRL")) ->data


toselect = c("Barcode","AreaName","Condition","Day","Batch","PercentageGBAandLAMP1")

data %>% #same 
  dplyr::select(toselect)->Table_All


#Mean of replicates

Table_All%>%
    pivot_longer(PercentageGBAandLAMP1,names_to = "PercentageGBAandLAMP1", values_to = "Measure") -> Table_all_long
Table_all_long2 = drop_na(Table_all_long)  

Table_all_long2 %>%
  group_by(Day,Condition,AreaName,Batch,PercentageGBAandLAMP1, Barcode) %>%
  summarize(MeanFeatures = mean(Measure)) -> Table_Mean


write.csv(Table_Mean, file = 'Table_plotting.csv')

```

Violin Plots
```{r}

Table_Mean%>%
  ggplot(aes(x = Condition, y=MeanFeatures),ordered=TRUE)+
  #geom_violin( aes(fill=Condition),show.legend = T, trim=T),
  geom_violin( aes(fill=Condition),show.legend = T,scale = "width", trim=F)+
  geom_dotplot(binaxis = "y",stackdir = "center",dotsize=0.8)+
  #scale_fill_manual(values= c("#bdd7e7","#2171b5"),name = "Condition", guide = FALSE)+
    scale_fill_manual(values= alpha(c("#1565C0","#CC0000"),0.75),name = "Condition",guide = "none")+
    theme(legend.key=element_blank()) +
    
  geom_signif(comparisons = list(c("CTRL", "GBA-PD")), test='wilcox.test',
              margin_top=0.5,vjust=0.5, size=0.5, textsize=9, map_signif_level=c("***"=0.001, "**"=0.01, "*"=0.05,  " "=2) ) +
  #facet_grid(~fct_relevel(Day, "d30","d60"), scales="free") +
       labs(x     ="",
        y     = "Percentage Colocalization",
       fill  = "Condition",
      title = " ") +
  theme_bw() +
  theme(
   axis.line = element_line(colour = 'black', size = 0.5) ,
    axis.title.x = element_blank(),
    axis.text.x = element_text(size=16, color="black"),
    axis.title.y = element_text(size = 16),
    axis.text.y = element_text(size=10, color="black"),
    axis.ticks.y = element_line(),
    axis.ticks.length=unit(.20, "cm"),
    #change legend text font size)
    #legend.key.size = unit(0.7, "cm"),
    #legend.key.width = unit(0.6,"cm"),
    legend.key=element_blank(),
    panel.grid.major = element_blank(), 
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    plot.title = element_text(size = 20, hjust=0.5, vjust= 1, face = "bold"),
    plot.subtitle = element_blank(),#element_text(size = 2, hjust=0.5)
    strip.text = element_text(size=12, vjust=0.5),
    strip.background = element_rect(fill="lightgray"),
   # panel.border = element_rect(fill = NA, color = "black"),
    panel.spacing.y = unit(0.8, "lines"),
    strip.switch.pad.wrap=unit(20, "lines"),
    legend.position="right",
    legend.text = element_text(size=17),
    legend.title = element_text(size=19)
    
  )  -> p
  #t<- cowplot::ggdraw(cowplot::add_sub(p, "Wilcox-test, ***p=0.001, **p=0.01, *p=0.05",hjust=-0.2, size=13))
  print(p)
  ##ggsave(paste0(Sys.Date(),"_", names[i], ".pdf"), plot=t)


ggsave(paste0(Sys.Date()," Colocalization GCase and LAMP1 DIV30.pdf"),height=3.5,width=3)

```
