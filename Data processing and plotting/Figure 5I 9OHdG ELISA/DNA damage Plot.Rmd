---
title: "R Notebook"
output: html_notebook
---

---
```{r}
library(ggplot2)
library(RColorBrewer)
library(tidyverse)
library(ggpubr)
library(ggsignif)
library(openxlsx)
library(viridis)
library(readxl)
```

```{r}
setwd("//atlas.uni.lux/users/isabel.rosety/GBA/8-OH-dG ELISA/")
#data <- read.csv("WB data for Rstudio plots.csv", header = T,  sep = ",")
data  <- read_excel("//atlas.uni.lux/users/isabel.rosety/GBA/8-OH-dG ELISA/20220914_8OHdG_ELISA/20220914_8OHdG_ELISA.xlsx",sheet=2)
OutPath="//atlas.uni.lux/users/isabel.rosety/GBA/8-OH-dG ELISA"

data <- as.data.frame(data)

```

Violin plots
```{r} 

data %>%
  #pivot_longer(cols=feature_names, names_to = "feature", values_to = "value") %>%
  #filter(feature %in% feature_names[i]) %>%
  ggplot( aes(x=factor(Condition, level = c("CTRL", "GBA-PD")), y=ConcentrationNN)) +
  #geom_violin( aes(fill=Condition),show.legend = T, trim=T),
  geom_violin( aes(fill=Condition),show.legend = T,scale = "width", trim=F)+
  geom_dotplot(binaxis = "y",stackdir = "center",dotsize=0.8)+
  #scale_fill_manual(values= c("#bdd7e7","#2171b5"),name = "Condition", guide = "none")+
   scale_fill_manual(values= alpha(c("#1565C0","#CC0000"),0.75),name = "Condition",guide = "none")+
     labs(x     ="",
        y     = "8-OHdG (ng/mL)",
       fill  = "Condition",
      title = "") +
    geom_signif(comparisons = list(c("CTRL", "GBA-PD")), test='wilcox.test', vjust=0.6, size=0.5, margin_top=0.5, 
    textsize=9, map_signif_level=c("***"=0.001, "**"=0.01, "*"=0.05,  " "=2) ) +
    #ggpubr::stat_compare_means(comparisons=my_comparisons, method="wilcox.test", p.adjust.method="BH",label="p.signif", label.x = 1.5)+
    theme_bw() +
  theme(
    axis.line = element_line(colour = 'black', size = 0.5) ,
    axis.title.x = element_blank(),
    axis.text.x = element_text(size=12, color="black"),
    axis.title.y = element_text(size = 12),
    axis.text.y = element_text(size=10, color="black"),
    axis.ticks.y = element_line(),
    axis.ticks.length=unit(.25, "cm"),
    #change legend text font size)
    #legend.key.size = unit(0.7, "cm"),
    #legend.key.width = unit(0.6,"cm"),
    legend.key=element_blank(),
    panel.grid.major = element_blank(), 
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    plot.title = element_text(size = 20, hjust=0.5, vjust= 1, face = "bold"),
    plot.subtitle = element_blank(),#element_text(size = 2, hjust=0.5)
    strip.text = element_text(size=12, vjust=0.5),
    strip.background = element_rect(fill="lightgray"),
   # panel.border = element_rect(fill = NA, color = "black"),
    panel.spacing.y = unit(0.8, "lines"),
    strip.switch.pad.wrap=unit(20, "lines"),
    legend.position="right",
    legend.text = element_text(size=17),
    legend.title = element_text(size=19)
  ) -> p
  t<- cowplot::ggdraw(cowplot::add_sub(p, "wilcox.test, ***p=0.001, **p=0.01, *p=0.05",hjust=-0.3, size=12))

   print(p)

#ggsave(paste0(Sys.Date()," dOHG - Day30.pdf"), plot=p, height=2.5,width=3, path=OutPath)
```
