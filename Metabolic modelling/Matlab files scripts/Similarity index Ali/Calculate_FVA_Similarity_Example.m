% (c) Thomas Sauter, 2021
%  DLSM, University of Luxembourg

addpath(genpath('~/cobratoolbox/'));
changeCobraSolver('ibm_cplex')

%Read the context speific models
model_1 = readCbModel('./model_1.mat');
model_2 = readCbModel('./model_2.mat');

% change the objective function for the 2 models
model_1 = changeObjective(model_1,'biomass_reaction');
model_2 = changeObjective(model_2,'biomass_reaction');
solution = optimizeCbModel(model_1,'max');
solution = optimizeCbModel(model_2,'max');

% Calculate the FVA
[minFlux_1,maxFlux_1] = fluxVariability(model_1,100);
[minFlux_2,maxFlux_2] = fluxVariability(model_2,100);

% Identify the union of rxns between the 2 models
union_rxns = union(model_1.rxns,model_2.rxns);

% Create an empty vector of zeros, then fill with the claculated fluxes for
% shared rxns
minFlux_1_imputed  = zeros(numel(union_rxns),0);
maxFlux_1_imputed  = zeros(numel(union_rxns),0);
minFlux_2_imputed  = zeros(numel(union_rxns),0);
maxFlux_2_imputed  = zeros(numel(union_rxns),0);

minFlux_1_imputed(find(ismember(union_rxns,model_1.rxns)),1) =  minFlux_1%(find(ismember(model_1.rxns,union_rxns)));
maxFlux_1_imputed(find(ismember(union_rxns,model_1.rxns)),1) =  maxFlux_1%(find(ismember(model_1.rxns,union_rxns)));
minFlux_2_imputed(find(ismember(union_rxns,model_2.rxns)),1) =  minFlux_2%(find(ismember(model_2.rxns,union_rxns)));
maxFlux_2_imputed(find(ismember(union_rxns,model_2.rxns)),1) =  maxFlux_2%(find(ismember(model_2.rxns,union_rxns)));

% Calculate the similarity on all reactions
Similarity = FVA_similarity_Thomas(minFlux_1_imputed, maxFlux_1_imputed, minFlux_2_imputed, maxFlux_2_imputed)
Similarity